from qeh.bb_calculator.chicalc import ChiHandler
from qeh.bb_calculator.bb_builder import interpolate_chi_to_bb
from qeh.bb_calculator.bb_interpolator import interpolate_building_blocks
from qeh.bb_calculator.chicalc import QPoint
import numpy as np
from ase.parallel import world
from qeh.tools.io import read
import pytest


class FragileBB(ChiHandler):
    def compare_and_expand_g(self, *args, **kwargs):
        if not hasattr(self, 'doom') and self.last_q_idx == 0:
            self.doom = 0
        self.doom += 1  # Advance doom
        print('doom', self.doom)
        if self.doom == 4:
            raise ValueError('Cthulhu awakens')
        return ChiHandler.compare_and_expand_g(self, *args, **kwargs)


def test_basics(testdir, get_dummy_chicalc):
    chicalc = get_dummy_chicalc()
    bb = ChiHandler('dummy', chicalc)
    bb.calculate_chi_2d()

    assert bb.complete
    assert bb.chicalc == chicalc

    assert bb.load_chi_file()
    assert bb.complete

    data = read('dummy.npz')

    assert np.amax(data['chi_qwgg']) == \
        pytest.approx(1, rel=1e-5)
    assert np.amax(data['Gz_g']) == \
        pytest.approx(6, rel=1e-2)


@pytest.mark.skipif(not world.size == 1, reason="needs single core")
def test_restart(testdir, get_dummy_chicalc):
    chicalc = get_dummy_chicalc()
    bb = ChiHandler('dummy', chicalc)
    bb.calculate_chi_2d()

    bbf = FragileBB('dummy_rs', chicalc,
                    restart=True)

    with pytest.raises(ValueError, match='Cthulhu*'):
        bbf.calculate_chi_2d()
    can_load = bbf.load_chi_file()
    assert can_load
    assert not bbf.complete
    bbf.calculate_chi_2d()
    can_load = bbf.load_chi_file()
    assert can_load
    assert bbf.complete

    data = read('dummy.npz')
    dataf = read('dummy_rs.npz')
    assert np.allclose(data['chi_qwgg'], dataf['chi_qwgg'])


def test_permutation(testdir, get_dummy_chicalc):
    chicalc = get_dummy_chicalc()
    bb = ChiHandler('dummy', chicalc)

    # Test permutation
    q_far_c = bb.Q_q[1].q_c + [1, 0, 0]
    q_far_v = q_far_c @ bb.chicalc.icell_cv
    P_rv = [np.array([0, 0, 0])]
    Q_far = QPoint(q_c=q_far_c, q_v=q_far_v, P_rv=P_rv)

    chi_F_wGG, G_F_Gv, _ = bb.chicalc.get_chi_wGG(Q_far)
    chi_wGG, G_Gv, _ = bb.chicalc.get_chi_wGG(bb.Q_q[1])
    chi_wPP, G_Pv = bb.permute_chi_and_G(chi_wGG, G_Gv, bb.Q_q[1].P_rv[1])
    G0 = np.argwhere(np.logical_and(
        np.isclose(G_F_Gv[:, 0], 0),
        np.isclose(G_F_Gv[:, 1], 0)))[:, 0]
    P0 = np.argwhere(np.logical_and(
        np.isclose(G_Pv[:, 0], 0),
        np.isclose(G_Pv[:, 1], 0)))[:, 0]
    chi_F_wgg = chi_F_wGG[:, G0, G0]
    chi_wgg = chi_wPP[:, P0, P0]

    assert chi_F_wgg == pytest.approx(chi_wgg)


def test_interpolation(testdir, get_dummy_chicalc):
    chicalc = get_dummy_chicalc()
    bb = ChiHandler('dummy', chicalc)
    bb.calculate_chi_2d()

    if world.rank == 0:
        interpolate_chi_to_bb('dummy',
                              q_grid=bb.q_abs_q,
                              aN=3)
    world.barrier()

    data = read('dummy-bb.npz')
    assert np.amax(data['chi_qwab']) == \
        pytest.approx(4.6903754420, rel=1e-5)
    assert np.amax(data['drho_qza']) == \
        pytest.approx(1.4926561671, rel=1e-2)

    if world.rank == 0:
        interpolate_building_blocks(BBfiles=['dummy-bb'])

    world.barrier()

    data = read('dummy-bb-int.npz')
    assert np.amax(data['chi_qwab']) == \
        pytest.approx(4.6903754420, rel=1e-5)
    assert np.amax(data['drho_qza']) == \
        pytest.approx(1.4926561671, rel=1e-2)
