import numpy as np
from conftest import make_bilayer
from qeh import QEH
import pytest


def test_eels(test_data):
    """Calculate eels spectra
    """
    from ase.units import Bohr, Hartree
    HS = make_bilayer(test_data.get_path('H-MoS2'),
                      test_data.get_path('H-WSe2'), wmax=10 / Hartree)

    q, w, eels_qw = HS.get_eels()
    previous_eelsmax = 0  # Needed for flake8...
    previous_wmax = 0

    for iq in range(10):
        i = eels_qw[iq, :].argmax()
        eelsmax = eels_qw[iq, i]
        wmax = w[i]
        if iq == 0:
            # Test values gamma point
            assert eelsmax == pytest.approx(156.61 / Bohr, rel=0.001)
            assert wmax == pytest.approx(2.958 / Hartree, abs=0.001)
        else:
            # Test so that perak position shifts to higher energy
            # and max value decreases
            assert eelsmax < previous_eelsmax
            assert wmax >= previous_wmax
        previous_eelsmax = eelsmax
        previous_wmax = wmax

    # Check so that all values are positive
    # but could be small negative parts due to numerics
    assert np.all(eels_qw >= -1e-6)


def test_eels_ml(test_data):
    """ Compares eels for different ML widths
    """
    from ase.units import Bohr
    d_list = np.array([6, 6.15, 6.3, 6.45, 6.6, 20, 50]) / Bohr
    previous_eelsmax = 0  # Needed for flake8...
    for i, d in enumerate(d_list):
        HS = QEH.heterostructure(BBfiles=[test_data.get_path('H-MoS2')],
                                 layerwidth_n=[d])

        q, w, eels_qw = HS.get_eels()
        if i > 0:
            assert previous_eelsmax == \
                pytest.approx(np.amax(eels_qw), rel=0.01)
        previous_eelsmax = np.amax(eels_qw)
