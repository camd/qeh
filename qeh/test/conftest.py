from contextlib import contextmanager
import pytest
import os
import numpy as np
from pathlib import Path
from ase.parallel import world, broadcast

from qeh.bb_calculator.bb_interpolator import interpolate_building_blocks
from qeh.bb_calculator.chicalc import ChiCalc, QPoint, SerialWComm
from qeh import QEH
from qeh.tools.io import read, write


try:
    from qehtestdata.qehtestdata import OldBuildingBlockData \
        as BuildingBlockData
except ImportError:
    pytest.exit('The required package qeh-test-data is not installed.'
                ' Please install it from'
                ' https://gitlab.com/camd/qeh-test-data.git'
                ' to run these tests.')


@contextmanager
def execute_in_tmp_path(request, tmp_path_factory):
    if world.rank == 0:
        # Obtain basename as
        # * request.function.__name__  for function fixture
        # * request.module.__name__    for module fixture
        basename = getattr(request, request.scope).__name__
        path = tmp_path_factory.mktemp(basename)
    else:
        path = None
    path = broadcast(path)
    cwd = os.getcwd()
    os.chdir(path)
    try:
        yield path
    finally:
        os.chdir(cwd)


@pytest.fixture(scope='function')
def testdir(request, tmp_path_factory):
    """Run test function in a temporary directory."""
    with execute_in_tmp_path(request, tmp_path_factory) as path:
        yield path


@pytest.fixture
def test_data():
    return BuildingBlockData()


@pytest.fixture
def get_dummy_chicalc(tmp_path):
    class DummyChicalc(ChiCalc):
        def __init__(self, icell_cv=None):
            if icell_cv is None:
                self.icell_cv = np.array([[2, 1, 0], [1, -2, 0], [0, 0, 2]])
            else:
                self.icell_cv = icell_cv

            self.L = 2 * np.pi / self.icell_cv[2, 2]
            self.omega_w = np.linspace(0, 1, 15)

            gn = 7
            G_g = np.arange(-(gn - 1) // 2, (gn + 1) // 2, 1)
            G_gggc = np.zeros((gn, gn, gn, 3), dtype=float)
            G_gggc[:, :, :, 0] = G_g[:, None, None]
            G_gggc[:, :, :, 1] = G_g[None, :, None]
            G_gggc[:, :, :, 2] = G_g[None, None, :]
            self.G_Gv = G_gggc.reshape(-1, 3) @ self.icell_cv
            tempG_v = self.G_Gv[0].copy()
            G0_G = np.isclose(self.G_Gv, np.array([0, 0, 0])).all(axis=1)
            self.G_Gv[0] = np.array([0, 0, 0])
            self.G_Gv[G0_G] = tempG_v

            self.GN = len(self.G_Gv)

            super().__init__()

        def get_q_grid(self, qmax: float | None = None):
            q_qc = np.zeros((10, 3), dtype=float)
            q_qc[:, 0] = np.linspace(0, 1, 10, endpoint=False)
            q_qc[0, 0] = 1e-7
            q_qv = q_qc @ self.icell_cv
            p_rv = [np.array([0, 0, 0]) @ self.icell_cv,
                    np.array([1, 0, 0]) @ self.icell_cv]

            return [QPoint(q_c=q_c, q_v=q_v, P_rv=p_rv)
                    for q_c, q_v in zip(q_qc, q_qv)]

        def get_chi_wGG(self, qpoint):
            R = np.array([1, 0.5, 1])
            GQ_Gv = self.G_Gv + qpoint.q_v
            chi_wGG = np.exp(-self.omega_w[:, None, None]) \
                * np.exp(+1j * GQ_Gv[None, :, None] @ R) \
                * np.exp(-1j * GQ_Gv[None, None, :] @ R)

            return chi_wGG, self.G_Gv, SerialWComm()

        def get_z_grid(self):
            return np.linspace(0, self.L, 100)

    return DummyChicalc


def make_bilayer(matA, matB, thick_A=11.8913, thick_B=12.6952, wmax=0):
    # Interpolate to same grid
    from ase.units import Bohr
    if world.rank == 0:
        interpolate_building_blocks(BBfiles=[matA, matB],
                                    q_grid=np.linspace(1e-5, 1 * Bohr, 50))
    world.barrier()
    nameA = Path(matA).stem
    intmatA = Path(matA).with_stem(f'{nameA}-int')
    nameB = Path(matB).stem
    intmatB = Path(matB).with_stem(f'{nameB}-int')
    qeh = QEH.heterostructure(BBfiles=[intmatA, intmatB],
                              layerwidth_n=[thick_A, thick_B],
                              qmax=None,
                              wmax=wmax,
                              amax=3)

    return qeh


def convert_old_bb_units(bbfilename, newbbfilename, Bohr, Hartree):
    # converts an old building block from atomic units to new units.
    # Bohr must be the value of 1 atomic Bohr in the new units
    # Hartree must be the value of 1 atomic Hartree in the new units
    # saves the converted bb with the specified filename
    data = read(bbfilename)
    drhoD_qz = data['drhoD_qz'] / Bohr**2
    omega_w = data['omega_w'] * Hartree
    drhoM_qz = data['drhoM_qz'] / Bohr
    q_abs = data['q_abs'] / Bohr
    chiD_qw = data['chiD_qw'] * Bohr
    chiM_qw = data['chiM_qw'] / Bohr
    z = data['z'] * Bohr
    newbb = {'drhoD_qz': drhoD_qz,
             'omega_w': omega_w,
             'drhoM_qz': drhoM_qz,
             'q_abs': q_abs,
             'chiD_qw': chiD_qw,
             'chiM_qw': chiM_qw,
             'z': z}
    write(newbbfilename, newbb, format='npz')
