import numpy as np
from qeh import QEH
from qeh.substrate import Substrate
import pytest
from ase.parallel import world


def mock_substrate(d):
    ei = 2.4
    f1 = 0.7514
    f2 = 0.1503
    f3 = 0.6011
    w1 = 0.055
    w2 = 0.098
    w3 = 0.140
    w = np.linspace(start=0, stop=1.3, num=2000)
    A = f1 * w1**2 / (w1**2 + w**2)
    B = f2 * w2**2 / (w2**2 + w**2)
    C = f3 * w3**2 / (w3**2 + w**2)
    eps_w = ei + A + B + C
    substrate = Substrate(eps_W=eps_w, omega_W=w,
                          dist=d, isotropic=True)
    return substrate


@pytest.mark.skipif(not world.size == 1, reason="needs single core")
def test_substrate(test_data):
    from ase.units import Bohr, Hartree
    mos2_bb = test_data.get_path('H-MoS2')
    # First test isolated layer
    d_MoS2 = 6.15 / Bohr
    hl_array = np.array([1, 0])
    el_array = np.array([1, 0])
    qeh = QEH.heterostructure(BBfiles=[mos2_bb],
                              layerwidth_n=[d_MoS2],
                              wmax=0,
                              amax=2)
    ee, ev = qeh.get_exciton_binding_energies(eff_mass=0.27,
                                              e_distr=el_array,
                                              h_distr=hl_array)
    Eb_isolated_layer = -ee[0]
    assert Eb_isolated_layer == pytest.approx(0.558478 / Hartree, rel=0.05)

    # Now add substrate
    substrate = mock_substrate(d_MoS2 * 0.51)
    HS_sub = QEH.heterostructure(BBfiles=[mos2_bb],
                                 layerwidth_n=[d_MoS2],
                                 wmax=0,
                                 amax=2,
                                 substrate=substrate)
    ee, ev = HS_sub.get_exciton_binding_energies(eff_mass=0.27,
                                                 e_distr=el_array,
                                                 h_distr=hl_array)
    Eb_sub = -ee[0]
    assert Eb_sub == pytest.approx(0.37587 / Hartree, rel=0.05)
    # Increase substrate distance and verify that exciton BE increases
    substrate = mock_substrate(2 * d_MoS2)
    HS_sub = QEH.heterostructure(BBfiles=[mos2_bb],
                                 layerwidth_n=[d_MoS2],
                                 wmax=0,
                                 amax=2,
                                 substrate=substrate)
    ee, ev = HS_sub.get_exciton_binding_energies(eff_mass=0.27,
                                                 e_distr=el_array,
                                                 h_distr=hl_array)
    Eb_sub2 = -ee[0]
    assert Eb_sub2 == pytest.approx(0.4263 / Hartree, rel=0.05)


def test_image_potential():
    eps_w = np.array([1])
    omega_w = np.array([0])
    dist = 5  # Bohr
    substrate = Substrate(eps_w, omega_w, dist)

    z0 = 0
    z_c = 2
    z_z = np.linspace(-10, 10, 101)
    q_q = np.linspace(0.01, 1, 11)
    dphi_qzi = np.exp(-np.abs(q_q[:, None, None] * z_z[:, None] - z_c))
    dphi_image_qzi = substrate.get_unscreened_image_potential(
        dphi_qzi, z_z, q_q, z0=z0)

    assert np.all(dphi_image_qzi[:, z_z < z0 - dist] == 0)
    assert np.allclose(dphi_image_qzi[:2, 51, 0], [-0.12221176, -0.04452075])
