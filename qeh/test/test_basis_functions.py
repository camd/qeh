from qehtestdata.qehtestdata import ChiData
import numpy as np
from qeh.bb_calculator.basis_functions import TrigBasis, SinPowerBasis
from qeh.bb_calculator.bb_builder import interpolate_chi_to_bb, ChiFile
from qeh.buildingblock import BuildingBlock
import pytest
from ase.parallel import world
from pathlib import Path


@pytest.mark.skipif(not world.size == 1, reason="needs single core")
def test_chi_to_bb_with_basis_functions():
    chipath = ChiData().get_path('MoS2')
    chifile = ChiFile.from_file(chipath)
    L = chifile.L
    bases = [TrigBasis(L=L),
             SinPowerBasis(L=L)]
    for i, basis in enumerate(bases):
        outfile = f'test_{i}.npz'

        # test generation of building block
        interpolate_chi_to_bb(file=chipath,
                              outfile=outfile,
                              zN=30,
                              aN=3,
                              q_grid=10,
                              w_grid=2,
                              basis=basis)
        # test loading of building block from generated file
        bb = BuildingBlock.from_file(outfile)

        # test that correct basis functions were used
        phi_qaz = basis(q_q=bb.q_q, aN=3, z_z=bb.Z_Z)
        assert np.array_equal(phi_qaz, bb.phi_qaZ)
        Path(outfile).unlink()
