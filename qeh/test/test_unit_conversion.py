import numpy as np
import pytest
from qeh import QEH
from ase.units import Bohr, Hartree
from pathlib import Path
from conftest import convert_old_bb_units


@pytest.mark.skip()
def test_unit_conversion(test_data):
    # convert building block from (Bohr, Hartree) to (Angstrom, eV),
    # and check that calculated values scale as expected
    bbfile = test_data.get_path('H-MoS2')
    newbbfile = 'MoS2-new-units-bb.npz'
    convert_old_bb_units(bbfile, newbbfile, Bohr=Bohr, Hartree=Hartree)

    N = 5
    qeh = QEH.heterostructure(BBfiles=[f'{N}{bbfile}'],
                              layerwidth_n=[6.15 / Bohr],
                              qmax=1 * Bohr,
                              wmax=0,
                              amax=2)
    qehnew = QEH.heterostructure(BBfiles=[f'{N}{newbbfile}'],
                                 layerwidth_n=[6.15],
                                 qmax=1,
                                 wmax=0,
                                 amax=2)
    Path(newbbfile).unlink()
    q_q, w, epsM_qw = qeh.get_macroscopic_dielectric_function()
    qnew_q, wnew_w, epsMnew_qw = qehnew.get_macroscopic_dielectric_function()
    assert np.allclose(q_q, qnew_q * Bohr)
    assert np.allclose(w, wnew_w * Hartree)
    assert np.allclose(epsM_qw, epsMnew_qw)
    q_q, w, epsM_w = qeh.get_macroscopic_dielectric_function(direction='z')
    qnew_q, wnew_w, epsMnew_w \
        = qehnew.get_macroscopic_dielectric_function(direction='z')
    assert np.allclose(epsM_w, epsMnew_w)
    e_distr_i = np.zeros(2 * N)
    e_distr_i[2] = 1
    h_distr_i = e_distr_i
    W_q = qeh.get_exciton_screened_potential(e_distr_i=e_distr_i,
                                             h_distr_i=h_distr_i)
    Wnew_q = qehnew.get_exciton_screened_potential(e_distr_i=e_distr_i,
                                                   h_distr_i=h_distr_i)
    assert np.allclose(W_q, Wnew_q / Bohr)
