from qeh import QEH
from qeh.materialparameters import default_thicknesses


def test_default_thickness(test_data):
    mos = test_data.get_path('H-MoS2')
    ws = test_data.get_path('H-WS2')
    qeh = QEH.heterostructure(BBfiles=[f'{5}{mos}', ws])
    assert qeh.hs.layers_l[0].layerwidth == \
        default_thicknesses['H-MoS2-icsd-644245']
    assert qeh.hs.layers_l[3].layerwidth == \
        default_thicknesses['H-MoS2-icsd-644245']
    assert qeh.hs.layers_l[5].layerwidth == \
        default_thicknesses['H-WS2-icsd-202366']
    assert qeh.hs.layers_l[4].layerwidth != qeh.hs.layers_l[5].layerwidth
