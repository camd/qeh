import numpy as np
from qeh.heterostructure import Heterostructure


def test_get_monopole_and_dipole_pots(test_data):
    # todo: add test containing bb with more basis functions
    from ase.units import Bohr
    plot = False  # plot potentials - useful for debugging
    bbfile = test_data.get_path('H-MoS2')
    het = Heterostructure(BBfiles=[f'3{bbfile}'],
                          layerwidth_n=[6.15 / Bohr],
                          qmax=1 * Bohr,
                          wmax=0,
                          amax=2)
    z_z = het.z_z

    Vmonopole_z = np.ones(z_z.shape)
    zM = np.array([layer.z0 for layer in het.layers_l]).mean()
    Vdipole_z = z_z - zM

    Vm_i = het.get_monopole_potential()
    Vd_i = het.get_dipole_potential()

    Vm_projected_z = Vm_i @ het.phi_qiz[0]
    Vd_projected_z = Vd_i @ het.phi_qiz[0]

    # monopole and dipole potentials should be exactly representable by the
    # layer basis functions inside the region where the basis functions are
    # nonzero.
    firstlayer, lastlayer = het.layers_l[0], het.layers_l[-1]
    zmin = firstlayer.z0 - firstlayer.layerwidth / 2
    zmax = lastlayer.z0 + lastlayer.layerwidth / 2
    inside_z = np.logical_and(z_z < zmax, z_z > zmin)
    if plot:
        import matplotlib.pyplot as plt
        plt.figure()
        plt.plot(z_z, Vm_projected_z)
        plt.plot(z_z, Vmonopole_z, ls='--')
        plt.axvline(zmin, color='k', ls='dotted')
        plt.axvline(zmax, color='k', ls='dotted')
        plt.savefig('Vmonopole.png')
        plt.close()

        plt.figure()
        plt.plot(z_z, Vd_projected_z)
        plt.plot(z_z, Vdipole_z, ls='--')
        plt.axvline(zmin, color='k', ls='dotted')
        plt.axvline(zmax, color='k', ls='dotted')
        plt.savefig('Vdipole.png')
        plt.close()

    assert np.allclose(Vmonopole_z[inside_z], Vm_projected_z[inside_z])
    assert np.allclose(Vdipole_z[inside_z], Vd_projected_z[inside_z])

    assert np.allclose(Vm_projected_z[~inside_z], 0)
    assert np.allclose(Vd_projected_z[~inside_z], 0)

    # test normalization
    Vmn_i = het.get_monopole_potential(normalize=True)
    Vdn_i = het.get_dipole_potential(normalize=True)
    Vmn_projected_z = Vmn_i @ het.phi_qiz[0]
    Vdn_projected_z = Vdn_i @ het.phi_qiz[0]

    dz = z_z[1] - z_z[0]
    m_normsq = dz * np.sum(Vmn_projected_z ** 2)
    d_normsq = dz * np.sum(Vdn_projected_z ** 2)

    assert np.isclose(m_normsq, 1)
    assert np.isclose(d_normsq, 1)
