import numpy as np
from conftest import make_bilayer
from ase.parallel import world
import pytest


@pytest.mark.skipif(not world.size == 1, reason="Parallelism not implemented")
def test_exciton_BE(test_data):
    """Calculate W(q)
    """
    mos2_path = test_data.get_path('H-MoS2')
    wse2_path = test_data.get_path('H-WSe2')
    qeh = make_bilayer(mos2_path, wse2_path)

    hl_array = np.array([1., 0., 0., 0.])
    el_array = np.array([0., 0., 1., 0.])
    inter_mass = 0.244
    ee, ev = qeh.get_exciton_binding_energies(eff_mass=inter_mass,
                                              e_distr=el_array,
                                              h_distr=hl_array)
    assert -ee[0] == pytest.approx(0.0112567, rel=1e-1)

    # test symmetrize
    ee, ev = qeh.get_exciton_binding_energies(eff_mass=inter_mass,
                                              e_distr=el_array,
                                              h_distr=hl_array,
                                              symmetrize=True)
    assert -ee[0] == pytest.approx(0.0112567, rel=1e-1)

    # test intralayer
    el_array = np.array([1., 0., 0., 0.])
    intra_mass = 0.236
    ee, ev = qeh.get_exciton_binding_energies(eff_mass=intra_mass,
                                              e_distr=el_array,
                                              h_distr=hl_array)

    assert -ee[0] == pytest.approx(0.0149074, rel=1e-1)
