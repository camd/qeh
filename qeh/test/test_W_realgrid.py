import numpy as np
import scipy as sp
from qeh import QEH
import pytest
from ase.parallel import world
from conftest import make_bilayer


@pytest.mark.skipif(not world.size == 1, reason="Parallelism not implemented")
def test_W_realgrid(test_data):
    # Tests the Fourier transform of the screened interaction.
    # The heterostructure is a dummy, and is only used to access
    # class functions
    mos2_path = test_data.get_path('H-MoS2')
    qeh = QEH.heterostructure(BBfiles=[f'{mos2_path}'],
                              layerwidth_n=[6.15],)
    hl_array = np.array([1., 0.])
    el_array = np.array([1., 0.])

    r0v = [10, 50, 100, 200]
    eps = 2
    for r0 in r0v:
        q_q = np.linspace(1e-12, 3, 1000)
        r_array = np.exp(np.linspace(np.log(1e-4), np.log(1000), 200))
        W_q = 1 / (q_q * (eps + q_q * r0))
        Wq_data = (q_q.copy(), -W_q.copy())
        r2_array, W_r = qeh.get_exciton_screened_potential_r(
            r_array=r_array, Wq_data=Wq_data,
            e_distr=el_array, h_distr=hl_array, intralayer=True)
        Wq_data = (q_q.copy(), -W_q.copy())

        W2_r = -1 / 4 / r0 * \
            (sp.special.struve(0, eps * r_array / r0) -
             sp.special.yv(0, eps * r_array / r0))

        '''plt.figure(figsize=(19.2, 10.8))
        plt.semilogx()
        plt.semilogy()
        plt.title(f'-W(r) for r0={r0}')
        plt.plot(r2_array, -W_r, linewidth=3, label='QEH')
        plt.plot(r_array, -W2_r, linewidth=3, label='Analytic')
        plt.plot(r_array, -W3_r, linewidth=1, label='New, smart integral')
        plt.legend()
        plt.savefig(f'W_r_{r0}.png')'''
        assert W_r == pytest.approx(W2_r, rel=5e-2)


@pytest.mark.skipif(not world.size == 1, reason="Parallelism not implemented")
def test_exciton_BE_arbdensity(test_data):
    """ Test calculate exciton binding energy with arbitrary e/h densities"""
    mos2_path = test_data.get_path('H-MoS2')
    wse2_path = test_data.get_path('H-WSe2')
    qeh = make_bilayer(mos2_path, wse2_path)

    hl_array = np.array([1., 0., 0, 0.])
    el_array = np.array([0., 0., 1., 0.])

    h_norm_q = (qeh.hs.drho_qzi @ hl_array).sum(1) * qeh.hs.dz
    e_norm_q = (qeh.hs.drho_qzi @ el_array).sum(1) * qeh.hs.dz

    # normalize densities so that they integrate to 1
    h_qz = qeh.hs.drho_qzi @ hl_array / h_norm_q[:, None]
    e_qz = qeh.hs.drho_qzi @ el_array / e_norm_q[:, None]

    # construct array of real-space positions.
    # we combine an exponential grid for low-r with a linear grid for large r
    r_r = np.exp(np.linspace(-10, 0, 100))
    dr = r_r[-1] - r_r[-2]
    r_r = np.concatenate((r_r, np.arange(1 + dr, 50, dr)))

    _, Wz_r = qeh.get_exciton_screened_potential_r(r_array=r_r,
                                                   e_distr=e_qz,
                                                   h_distr=h_qz,
                                                   intralayer=False)
    _, W_r = qeh.get_exciton_screened_potential_r(r_array=r_r,
                                                  e_distr=el_array,
                                                  h_distr=hl_array,
                                                  intralayer=False)

    assert Wz_r == pytest.approx(W_r, abs=1e-3, rel=1e-5)
    # test intralayer

    _, Wintraz_r = qeh.get_exciton_screened_potential_r(r_array=r_r,
                                                        e_distr=h_qz,
                                                        h_distr=h_qz,
                                                        intralayer=True)
    _, Wintra_r = qeh.get_exciton_screened_potential_r(r_array=r_r,
                                                       e_distr=hl_array,
                                                       h_distr=hl_array,
                                                       intralayer=True)
    # here we need a slightly higher tolerance, as W_intra diverges in
    # the r-> 0 limit, and therefore is a little more volatile numerically
    assert Wintraz_r == pytest.approx(Wintra_r, abs=5e-3, rel=1e-5)
