import pytest
import numpy as np
from ase.parallel import world
from ase.build import mx2

from qeh.bb_calculator.chicalc import ChiHandler
from qeh.bb_calculator.bb_interpolator import \
    interpolate_building_blocks
from qeh.bb_calculator.bb_builder import interpolate_chi_to_bb
from qeh.tools.bb_plotter import plot_bb
from qeh import QEH
from qeh.heterostructure import Heterostructure
from qeh.tools.io import read


debug = False


class FragileBB(ChiHandler):
    def compare_and_expand_g(self, *args, **kwargs):
        if not hasattr(self, 'doom') and self.last_q_idx == 0:
            self.doom = 0
        self.doom += 1  # Advance doom
        print('doom', self.doom)
        if self.doom == 4:
            raise ValueError('Cthulhu awakens')
        return ChiHandler.compare_and_expand_g(self, *args, **kwargs)


# Test the full GPAW integration
@pytest.mark.skipif(not world.size == 1, reason="needs single core")
def test_basics(testdir, test_data):
    pytest.importorskip('gpaw')

    from gpaw.response.df import DielectricFunction
    from gpaw.response.qeh import QEHChiCalc
    from gpaw import GPAW, PW, FermiDirac
    from ase.units import Bohr, Hartree

    calc = GPAW(mode=PW(150),
                xc='LDA',
                occupations=FermiDirac(width=0.05),
                kpts={'size': (6, 6, 1), 'gamma': True},
                txt=None)
    calc.atoms = mx2(formula='MoS2', a=3.184, thickness=3.13, vacuum=5.0)
    calc.get_potential_energy()

    def dielectric(domega, omega2, ecut=10):
        diel = DielectricFunction(calc=calc,
                                  frequencies={'type': 'nonlinear',
                                               'omegamax': 10,
                                               'domega0': domega,
                                               'omega2': omega2},
                                  eta=0.001,
                                  nblocks=world.size,
                                  ecut=ecut,
                                  truncation='2D',
                                  txt=None)
        return diel

    df = dielectric(0.1, 0.5, ecut=80)

    # Testing to compute building block
    chicalc = QEHChiCalc(df)
    bb = ChiHandler('mos2', chicalc, q_max=2.3)
    bb.calculate_chi_2d()

    if debug:
        plot_bb(f'{testdir}/mos2')
        plot_bb(test_data.get_path('H-MoS2'))

    # Test restart calculation
    bbf = FragileBB('mos2_rs', chicalc, q_max=2.3,
                    restart=True)
    with pytest.raises(ValueError, match='Cthulhu*'):
        bbf.calculate_chi_2d()
    can_load = bbf.load_chi_file()
    assert can_load
    assert not bbf.complete
    bbf.calculate_chi_2d()
    can_load = bbf.load_chi_file()
    assert can_load
    assert bbf.complete
    data = read('mos2.npz')
    dataf = read('mos2_rs.npz')

    assert np.allclose(data['chi_qwgg'], dataf['chi_qwgg'])

    interpolate_chi_to_bb(file='mos2',
                          outfile='mos2-bb',
                          q_grid=np.linspace(0, 1, 25) * Bohr,
                          w_grid=np.linspace(0, 1, 25) / Hartree,
                          aN=2)

    # Test equal building blocks
    hs = Heterostructure(BBfiles=['2mos2-bb'],
                         layerwidth_n=[5 / Bohr],
                         wmax=0)
    qeh = QEH(hs=hs)
    chi = qeh.get_chi_matrix()

    hs = Heterostructure(BBfiles=['mos2-bb', 'mos2-bb'],
                         layerwidth_n=[5 / Bohr],
                         wmax=0)
    qeh = QEH(hs=hs)
    chi_new = qeh.get_chi_matrix()
    assert np.allclose(chi, chi_new)

    correct_val = 0.038062237543439105 + 2.029511643570192e-07j

    assert np.amax(chi) == pytest.approx(correct_val, rel=0.1)


@pytest.mark.skipif(not world.size >= 4, reason="expensive")
def test_compare(testdir, test_data):
    pytest.importorskip('gpaw')

    from gpaw.response.df import DielectricFunction
    from gpaw.response.qeh import QEHChiCalc
    from gpaw import GPAW, PW, FermiDirac
    from ase.units import Bohr

    calc = GPAW(mode=PW(150),
                xc='PBE',
                occupations=FermiDirac(width=0.05),
                kpts={'size': (36, 6, 1), 'gamma': True},
                parallel={'domain': 1})
    calc.atoms = mx2(formula='MoS2', a=3.184, thickness=3.13, vacuum=8.0)
    calc.get_potential_energy()
    # calc.diagonalize_full_hamiltonian(nbands=500, expert=True)
    calc.write('mos2.gpw', 'all')
    world.barrier()

    def dielectric(domega, omega2, ecut=10):
        diel = DielectricFunction(calc='mos2.gpw',
                                  frequencies=np.array([0, 0.1]),
                                  nblocks=world.size // 2,
                                  eta=0.001,
                                  ecut=ecut,
                                  hilbert=False,
                                  truncation='2D',
                                  txt=None)
        return diel

    df = dielectric(0.05, 3, ecut=10)

    chicalc = QEHChiCalc(df, qinf_rel=1e-12)
    bb = ChiHandler('mos2', chicalc, q_max=0.6)
    bb.calculate_chi_2d()

    if world.rank == 0 and debug:
        plot_bb(f'{testdir}/mos2', outputpath='calc')
        plot_bb(test_data.get_path('H-MoS2'), outputpath='pre')
    world.barrier()

    # Compare calculated BB to old BB:
    bbfile = test_data.get_path('H-MoS2')[:-4]

    if world.rank == 0:
        interpolate_chi_to_bb(file='mos2')
        interpolate_building_blocks(BBfiles=[bbfile, 'mos2-bb'],
                                    q_grid=np.linspace(0, 0.6, 60) * Bohr)
    world.barrier()

    qeh_old = QEH.heterostructure(BBfiles=[f'{1}{bbfile}{"-int"}'],
                                  layerwidth_n=[6.15 / Bohr],
                                  wmax=0,
                                  amax=2)

    qeh_new = QEH.heterostructure(BBfiles=[f'{1}{"mos2-bb-int"}'],
                                  layerwidth_n=[6.15 / Bohr],
                                  wmax=0,
                                  amax=2)

    q_q_old, w_old, epsM_old = qeh_old.get_macroscopic_dielectric_function()
    q_q_new, w_new, epsM_new = qeh_new.get_macroscopic_dielectric_function()

    assert epsM_new[0, 0] == pytest.approx(1, rel=1e-4)
    assert epsM_new[1:] == pytest.approx(epsM_old[1:], rel=0.2)
