from scipy.interpolate import CubicSpline
import numpy as np
from scipy.special import j0
from qeh.tools.integrate_j0_spline import integrate_j0_spline
from scipy import integrate
import matplotlib.pyplot as plt
import pytest


def test_bessel_integral():
    plot = False
    r0 = 10
    q_q = np.linspace(1e-5, 10, 200)
    r_r = np.exp(np.linspace(-5, 5, 100))
    W_q = 1 / q_q * 1 / (1 + r0 * q_q)
    WR_q = CubicSpline(q_q, W_q * q_q)

    res_r = integrate_j0_spline(spl=WR_q, r_r=r_r)

    W_r = np.zeros_like(r_r)
    q0 = q_q.max()

    for i, r in enumerate(r_r):
        def WR_qr(q):
            return WR_q(q) * j0(q * r)

        W_r[i] = integrate.quad(WR_qr, a=0.0, b=q0, limit=1000)[0]

    assert res_r == pytest.approx(W_r, abs=2e-5)
    if plot:
        plt.semilogx(r_r, res_r, lw=2)
        plt.semilogx(r_r, W_r, color='k')

        plt.show()

        plt.figure()
        plt.semilogx(r_r, res_r - W_r)
        plt.show()
