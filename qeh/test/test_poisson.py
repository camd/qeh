import numpy as np
from qeh import QEH
from qeh.heterostructure import Heterostructure


def test_poisson(test_data):
    bbfile = test_data.get_path('H-MoS2')
    hs = Heterostructure(BBfiles=[bbfile],  # set up structure
                         layerwidth_n=[6.15],  # layer width array
                         wmax=0,  # only include w=0
                         qmax=1,  # q grid up to 1 Ang^{-1}
                         )
    qeh = QEH(hs=hs)

    z_z = np.linspace(-np.pi / 2, np.pi / 2, 101)
    drho_z = np.cos(z_z)
    q = 0.1
    v_z = qeh.solve_poisson_1D(drho_z, q, z_z)
    # test solution of poisson equation by comparison to analytic solution
    assert np.allclose(v_z[90], 111.0188477, atol=1e-1)

    q2 = 1e-12

    v2_z = qeh.solve_poisson_1D(drho_z, q2, z_z)

    assert np.allclose(v2_z[50], 1.256637062e13, rtol=1e-4)
