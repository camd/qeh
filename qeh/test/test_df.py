import pytest
from qeh import QEH
from qeh.bb_calculator.bb_interpolator import interpolate_building_blocks
import numpy as np
from ase.parallel import world
from qeh.qwcomm import QCommMPI


def test_df_mos2(test_data):
    from ase.units import Bohr
    """Calculate static dielectric function for multilayer MoS2
    for 1 to 20 layers
    """
    # positions of maximum:
    q1 = 100000000
    e1 = 0.0

    qN = 100
    bbfile = test_data.get_path('H-MoS2')
    if bbfile[-4:] == '.npz':
        bbfile = bbfile[:-4]

    if world.rank == 0:
        interpolate_building_blocks(BBfiles=[bbfile],
                                    q_grid=np.linspace(1e-5, 1 * Bohr, qN))

    world.barrier()
    epsMz_n = []
    for n in [1, 2, 3, 4, 5, 10, 20]:
        qeh = QEH.heterostructure(BBfiles=[f'{n}{bbfile}-int'],
                                  layerwidth_n=[6.15 / Bohr],
                                  wmax=0,
                                  amax=3,
                                  qcomm=QCommMPI(qN))
        q_q, w, epsM = qeh.get_macroscopic_dielectric_function()
        _, _, epsMz_w = qeh.get_macroscopic_dielectric_function(direction='z')
        epsMz_n.append(epsMz_w[0])
        i = epsM.real.argmax()
        q2 = q_q[i]
        e2 = epsM.real[i, 0]
        assert q2 <= q1  # should decrease as a function of n
        assert e2 > e1  # should increase as a function of n
        assert epsM[0] == pytest.approx(1, rel=5e-2)
        q1 = q2
        e1 = e2

    # "Bulk" values:
    assert q1 == pytest.approx(0.1111 * Bohr, abs=0.001)
    assert e1 == pytest.approx(12.360, abs=0.01)
    print(epsMz_n)
    correct_epsMz_n = np.array(
        [5.1881, 4.9893, 4.9274, 4.8969, 4.8792, 4.8432, 4.8234])
    assert np.allclose(correct_epsMz_n, epsMz_n, atol=1e-4)


@pytest.mark.skipif(not world.size == 1, reason="needs single core")
def test_get_inverse_dielectric_function(test_data):
    from ase.units import Bohr
    bbfile = test_data.get_path('H-MoS2')
    qeh = QEH.heterostructure(BBfiles=[f'2{bbfile}'],
                              layerwidth_n=[6.15 / Bohr])
    iq_q = [4, 5]
    iw_w = [31, 0, 2]
    epsinv_QW = qeh.get_inverse_dielectric_matrix(iq_q=iq_q, iw_w=iw_w)
    epsinv_qw = qeh.get_inverse_dielectric_matrix()

    epsinv_Qw = epsinv_qw[iq_q]
    assert np.all(epsinv_Qw[:, iw_w] == epsinv_QW)

    # test single-q calculation
    epsinv_w = qeh.get_inverse_dielectric_matrix(iq_q=[15])
    assert np.all(epsinv_w == epsinv_qw[15])
