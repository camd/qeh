import numpy as np
if np.__version__ < '2.0.0':
    from numpy import trapz as trapezoid
else:
    from numpy import trapezoid
from conftest import make_bilayer
from ase.parallel import world
import pytest


@pytest.mark.skipif(not world.size == 1, reason="Parallelism not implemented")
def test_W(test_data):
    """Calculate W(q)
    """
    mos2_path = test_data.get_path('H-MoS2')
    ws2_path = test_data.get_path('H-WS2')
    qeh = make_bilayer(mos2_path, ws2_path)
    hl_array = np.array([1., 0., 0., 0.])
    el_array = np.array([1., 0., 0., 0.])

    # intralayer exciton potential
    Wintra_q = qeh.get_exciton_screened_potential(el_array, hl_array)

    # average intralayer screened potential
    Wintra_qw = qeh.get_screened_potential(layer=0)

    # Exciton potential should be close minus to average potential when
    # re-scaled properly. Below we rescale Wintra_qw for the comparison.
    # We don't expect them to be quite the same, because they are
    # calculated using different bases:
    # Wintra_qw = < phi|W|rho>,
    # Wintra_q = <rho|W|rho>
    dZ = qeh.hs.layers_l[0].z_Z[1] - qeh.hs.layers_l[0].z_Z[0]
    norm_rhom_q = trapezoid(
        qeh.hs.qcomm.all_gather_qX(qeh.hs.layers_l[0].bb.drho_qZa[:, :, 0]),
        axis=1, dx=dZ)
    gphi_qij = qeh.hs.qcomm.all_gather_qX(qeh.hs.gphi_qij)
    Wintra_qw = Wintra_qw * gphi_qij[:, 0, 0] / norm_rhom_q[:, None]

    assert Wintra_q == pytest.approx(-Wintra_qw[:, 0], rel=0.25)

    # Check some numbers as well...
    assert np.array([Wintra_q[0], Wintra_q[20], Wintra_q[40]]) == \
        pytest.approx(np.array([-628226.2754553141, -2.272712338544286,
                                -1.154146966008974]), rel=5e-2)

    # Getting the interlayer exciton screened potential
    # on real grid
    hl_array = np.array([0., 0., 1., 0.])
    el_array = np.array([1., 0., 0., 0.])

    r, W_r = qeh.get_exciton_screened_potential_r(
        r_array=np.linspace(1e-1, 30, 1000),
        e_distr=el_array,
        h_distr=hl_array)
    assert np.array([W_r[0], W_r[-1]]) == \
        pytest.approx([-0.01861, -0.012705402345737464],
                      rel=0.05)

    # test solution on real-space grid

    h_distr_qz = qeh.hs.drho_qzi[:, :, 0] / norm_rhom_q[:, None]
    e_distr_qz = h_distr_qz.copy()
    W_q = qeh.get_exciton_screened_potential(e_distr_qz, h_distr_qz)
    assert W_q == pytest.approx(Wintra_q, rel=0.05)
