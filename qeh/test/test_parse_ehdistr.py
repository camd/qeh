import numpy as np
from qeh import QEH


def test_parse_ehdistr(test_data):
    mos2_path = test_data.get_path('H-MoS2')
    qeh = QEH.heterostructure(BBfiles=[f'{5}{mos2_path}'], amax=2)
    e_distr = 3
    parsed_distr = np.zeros(qeh.hs.ndim)
    parsed_distr[2 * e_distr] = 1
    dct = qeh.parse_ehdistr(e_distr)
    assert dct['type'] == 'monopole'
    assert np.all(dct['distr'] == parsed_distr)

    e_distr = [1, 0, 1, 0, 1]
    parsed_distr = np.zeros(qeh.hs.ndim)
    parsed_distr[0] = 1
    parsed_distr[4] = 1
    parsed_distr[8] = 1
    dct = qeh.parse_ehdistr(e_distr)
    assert dct['type'] == 'monopole'
    assert np.all(dct['distr'] == parsed_distr)

    zN = len(qeh.hs.z_z)
    f_z = np.ones(zN)

    dct = qeh.parse_ehdistr(f_z)
    assert dct['type'] == 'grid'
    assert np.all(dct['distr'] == np.atleast_2d(f_z))

    qN = len(qeh.hs.q_q)
    f_qz = np.ones((qN, zN))

    dct = qeh.parse_ehdistr(f_qz)
    assert dct['type'] == 'grid'
    assert np.all(dct['distr'] == f_qz)
