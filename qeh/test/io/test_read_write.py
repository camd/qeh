from qeh.tools.io import read, write
from ase import Atoms
import numpy as np
from pathlib import Path
import pytest
from ase.parallel import world


def _test_read_write(format):
    simpledct = {'number': 3.0,
                 'string': 'hello',
                 'array': np.array([1.0, 2.0]),
                 'npfloat': np.array(3)}
    filename = f'test.{format}'
    write(filename, simpledct)
    world.barrier()
    newdct = read(filename)

    assert dicts_equal(simpledct, newdct)

    nested_dict = {'int': 700,
                   'subdict': simpledct}
    filename2 = f'test2.{format}'
    write(filename2, nested_dict, format=format)
    world.barrier()
    newdct2 = read(filename2, format=format)

    assert dicts_equal(nested_dict, newdct2)

    atoms = Atoms('H2')
    array = np.linspace(1, 16, 16).reshape(4, 2, 2) * np.exp(1j * np.pi / 3)
    complicated_dict = {'atoms': atoms.todict(),
                        'complexarray': array,
                        'nested_dict': nested_dict}
    filename3 = f'test3.{format}'
    write(filename3, complicated_dict, format=format)
    world.barrier()
    newdct3 = read(filename3, format=format)

    assert dicts_equal(complicated_dict, newdct3)

    write(filename2, simpledct, format=format)  # test that overwrite works
    world.barrier()
    Path(filename).unlink(missing_ok=True)
    Path(filename2).unlink(missing_ok=True)
    Path(filename3).unlink(missing_ok=True)


def test_json():
    pytest.importorskip('json')
    _test_read_write(format='json')


def test_npz():
    _test_read_write(format='npz')


def test_hdf5():
    pytest.importorskip('h5py')
    _test_read_write(format='hdf5')
    _test_read_write(format='h5')


def test_unsupported_type():
    with pytest.raises(ValueError):
        _test_read_write(format='h4')


def dicts_equal(dict1, dict2):
    if dict1.keys() != dict2.keys():
        return False

    for key in dict1:
        val1, val2 = dict1[key], dict2[key]

        if isinstance(val1, dict) and isinstance(val2, dict):
            if not dicts_equal(val1, val2):
                return False

        elif isinstance(val1, np.ndarray) and isinstance(val2, np.ndarray):
            if not np.array_equal(val1, val2):
                return False

        else:
            if val1 != val2:
                return False

    return True
