from abc import ABC, abstractmethod
import numpy as np
from dataclasses import dataclass, field
from ase.parallel import world
from typing import Union
from pathlib import Path
from qeh.tools.io import read, write


@dataclass
class QPoint:
    q_c: np.ndarray
    q_v: np.ndarray
    P_rv: list[np.ndarray] = \
        field(default_factory=lambda: [np.array([0, 0, 0])])


class SerialWComm:
    def gather(self, data_wX, root=0):
        return data_wX


class ChiCalc(ABC):
    ''' Abstract class for calculating the dielectric function

    Required Properties
    ----------
    L : float
        the out-of-plane lattice parameter [unit: Bohr]
    omega_w : numpy array
        the frequencies [unit: Hartree]
    '''

    L = None
    omega_w = None

    @abstractmethod
    def get_chi_wGG(self, qpoint: QPoint):
        ''' Returns the dielectric function

        Parameters
        ----------
        qpoint : QPoint
            the momentum vector dataclass for
            a given q-point

        Returns
        -------
        chi_wGG : numpy array
            the dielectric function on rank 0.
        G_Gv : numpy array
            the G-vectors sorted such that G_Gv[0] = (0, 0, 0).
            [unit: 1/Bohr]
        WComm : SerialWComm | MPI Communicator Object
            the serial or MPI communicator object must
            have a gather method which takes a numpy array
            distributed along the first dimension and gathers
            it on the root processor.
        '''

        raise NotImplementedError()

    @abstractmethod
    def get_q_grid(self, qmax: float | None = None):
        ''' Returns the q grid in two numpy arrays

        Parameters
        ----------
        qmax : float | None
            the qmax [unit: 1/Bohr]. If none is specified,
            it will be equal the BZ edge. If qmax is larger
            than the BZ edge, it will be set to the BZ edge.

        Returns
        -------
        q_pts : list[QPoint]
            list of QPoints for the q-points
        '''

        raise NotImplementedError()

    @abstractmethod
    def get_z_grid(self):
        ''' Returns a z grid in a numpy array
        as given by the ground state calculator

        Returns
        -------
        z_z : numpy array
            the z grid [unit: Bohr]
        '''

        raise NotImplementedError()


class ChiHandler:
    def __init__(self,
                 filename: Union[Path, str],
                 chicalc: ChiCalc,
                 q_max: float | None = None,
                 restart: bool = False):
        ''' Initialize the building block calculator

        Parameters
        ----------
        filename : Path or str
            the filename to store the data, as well as load
            the data if restart is True.
        chicalc : ChiCalc
            the chi calculator object.
        q_max : float | None
            the q-vector cutoff. If set to none, the q-vector
            cutoff will correspond to the BZ edge. [unit: 1/Bohr]
        restart : bool
            whether to restart the calculation from the last
            stored data in the file specified by filename.
        '''

        self.chicalc = chicalc
        self.filename = filename
        self.q_max = q_max
        self.Q_q = chicalc.get_q_grid(q_max)

        # Number of Q-points to calculate chi_wGG for
        self.Qn = len(self.Q_q)

        # Total number for q-points including G permutations
        self.qn = sum([len(Q.P_rv) for Q in self.Q_q])

        # Initialize the chi and rho arrays
        nw = len(self.chicalc.omega_w) if world.rank == 0 else 0

        self.chi_qwgg = np.zeros([self.qn, nw, 1, 1],
                                 dtype=complex)
        self.Gz_g = np.array([0])
        self.q_abs_q = np.zeros([self.qn], dtype=float)

        self.complete = False
        self.last_q_idx = 0
        if restart:
            if self.load_chi_file():
                print(f'Loaded chi file from {self.filename}')
            else:
                print(f'Failed to load chi file from {self.filename}')

    def calculate_chi_2d(self):
        """
        Calculate the building block contribution for each q-point.

        This function iterates over each q-point in the `Q_q` list
        and calculates the contribution to the building block.
        It prints the current q-point being calculated and then
        calculates the `chi_wGG` and `G_Gv` using the `get_chi_wGG`
        method of the `chicalc` object.
        It adds the `G_Gv` and `chi_wGG` to the corresponding arrays
        and saves the intermediate building block file.

        After all q-points have been calculated, it sets the `complete`
        flag to True.

        Finally, it saves the complete building block file.

        Parameters:
            None

        Returns:
            None
        """

        if self.complete:
            return

        for current_q_idx in range(self.last_q_idx, self.Qn):
            QPt = self.Q_q[current_q_idx]

            qcstr = '(' + ', '.join(['%.3f' % x for x in QPt.q_c]) + ')'
            print(  # XXX: Improve printing later
                'Calculating contribution from q-point #%d/%d, q_c=%s' % (
                    current_q_idx + 1, self.Qn, qcstr), flush=False)
            # Calculate chi_wGG, distributed over frequencies
            chi_wGG, G_Gv, wcomm = self.chicalc.get_chi_wGG(QPt)

            for i, P_v in enumerate(QPt.P_rv):
                # Permute chi_wGG and G_Gv
                chi_wPP, G_Pv = self.permute_chi_and_G(chi_wGG, G_Gv, P_v)

                # Calculate the monopole and dipole contribution
                # Useful quantities
                P0 = np.argwhere(np.logical_and(
                    np.isclose(G_Pv[:, 0], 0),
                    np.isclose(G_Pv[:, 1], 0)))[:, 0]
                if len(P0) < 2:
                    raise ValueError('E-cut too low for specified q_max.')
                G0z_g = G_Pv[P0, 2]
                chi_wgg = chi_wPP[:, P0[:, None], P0[None, :]]
                ind_g = self.compare_and_expand_g(G0z_g)

                # Gather on rank 0
                # XXX: If memory is still an issue, more parallelization
                # could be done here
                chi_wgg = wcomm.gather(chi_wgg, root=0)

                if world.rank == 0:
                    self.chi_qwgg[current_q_idx + i * self.Qn,
                                  :, ind_g[:, None], ind_g[None, :]] = \
                        chi_wgg.transpose(1, 2, 0)
                    self.q_abs_q[current_q_idx + i * self.Qn] = \
                        np.linalg.norm(QPt.q_v + P_v)

            if world.rank == 0:
                self.save_chi_file(q_idx=current_q_idx + 1)
            world.barrier()
            self.last_q_idx = current_q_idx + 1

        self.complete = True

        if world.rank == 0:
            self.save_chi_file()

        world.barrier()

    def compare_and_expand_g(self, Gz_h):
        ind_h = []
        for Gz in Gz_h:
            equal = np.isclose(Gz, self.Gz_g)
            if not np.any(equal):
                ind_h.append(len(self.Gz_g))
                self.Gz_g = np.append(self.Gz_g, Gz)
                qwgg = np.array(self.chi_qwgg.shape)
                qwgg[2:] = len(self.Gz_g)
                chi_qwgg = np.zeros_like(self.chi_qwgg, shape=qwgg)
                chi_qwgg[:, :, :-1, :-1] = self.chi_qwgg
                self.chi_qwgg = chi_qwgg
            else:
                ind_h.append(np.where(equal)[0][0])

        return np.array(ind_h)

    def permute_chi_and_G(self,
                          chi_wGG: np.ndarray,
                          G_Gv: np.ndarray,
                          P_v: np.ndarray):
        r"""Permute the chi and G arrays according to the
        reciprocal lattice vector P_v.

        Parameters
        ----------
        chi_wGG : numpy array
            the chi array
        G_Gv : numpy array
            the G-vectors
        P_v : numpy array
            the reciprocal lattice vector
        """

        GP_Gv = G_Gv + P_v
        M_GG = np.isclose(GP_Gv[:, None, :], G_Gv[None, :, :]).all(axis=2)
        _, Perm_G = np.where(M_GG)
        if len(Perm_G) == 0:
            raise ValueError('E-cut too low for specified q_max.')
        chi_wPP = chi_wGG[:, Perm_G[:, None], Perm_G[None, :]]
        G_Pv = G_Gv[_]
        assert np.allclose(GP_Gv[_], G_Gv[Perm_G])
        return chi_wPP, G_Pv

    def save_chi_file(self, filename=None, q_idx=None, format=None):
        if q_idx is None:
            q_idx = self.last_q_idx
        if filename is None:
            filename = self.filename

        data = {'last_q': q_idx,
                'complete': self.complete,
                'q_q': self.q_abs_q,
                'L': self.chicalc.L,
                'omega_w': self.chicalc.omega_w,
                'chi_qwgg': self.chi_qwgg,
                'Gz_g': self.Gz_g}

        return write(filename, data, format)

    def load_chi_file(self, format=None):
        try:
            data = read(self.filename)
        except OSError:
            return False

        if (data['omega_w'] == self.chicalc.omega_w).all() and \
           (data['L'] == self.chicalc.L):

            self.last_q_idx = data['last_q']
            self.complete = data['complete']
            self.Gz_g = data['Gz_g']

            if world.rank == 0:
                self.chi_qwgg = data['chi_qwgg']
                self.q_abs_q = data['q_q']

            return True
        else:
            return False
