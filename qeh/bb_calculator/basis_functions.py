from abc import ABC, abstractmethod
import numpy as np


class PotentialBasis(ABC):
    """
    Class for defining potential basis functions to be used in the BBBuilder
    """

    @abstractmethod
    def __call__(self, q_q: np.ndarray, aN: int, z_z: np.ndarray):
        raise NotImplementedError()


class TrigBasis(PotentialBasis):
    def __init__(self, L):
        self.L = L

    def __call__(self, q_q: np.ndarray, aN: int, z_z: np.ndarray):
        qN = q_q.size
        zN = z_z.size
        phi_qaz = np.zeros((qN, aN, zN))
        for a in range(aN):
            phi_qaz[:, a] = self.func(a, z_z, q_q)
        return phi_qaz

    def func(self, a: int, z_z: np.ndarray, q_q: np.ndarray):
        if a % 2 == 0:
            w = a // 2
            phi_qz = np.cos(2 * np.pi * w * z_z / self.L)
        else:
            w = (a + 1) // 2
            phi_qz = np.sin(2 * np.pi * w * z_z / self.L)

        phi_qz = np.tile(phi_qz, reps=(q_q.size, 1))
        return phi_qz


class PolynomialBasis(PotentialBasis):

    def __call__(self, q_q: np.ndarray, aN: int, z_z: np.ndarray):
        phi_az = z_z ** np.arange(aN)[:, None]
        phi_qaz = np.tile(phi_az, reps=(q_q.size, 1, 1))
        return phi_qaz


class SinPowerBasis(PotentialBasis):
    def __init__(self, L):
        self.L = L

    def __call__(self, q_q: np.ndarray, aN: int, z_z: np.ndarray):
        phi_az = np.sin(2 * np.pi * z_z / self.L)**np.arange(aN)[:, None]
        phi_qaz = np.tile(phi_az, reps=(q_q.size, 1, 1))
        return phi_qaz
