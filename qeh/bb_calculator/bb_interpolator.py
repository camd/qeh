from scipy.interpolate import CubicSpline
import numpy as np
if np.__version__ < '2.0.0':
    from numpy import trapz as trapezoid
else:
    from numpy import trapezoid
from dataclasses import dataclass
from qeh.tools.io import read, write
from ase.parallel import world
from typing import Union
from pathlib import Path


@dataclass
class BBData:
    q_q: np.ndarray
    chi_qwab: np.ndarray
    drho_qza: np.ndarray
    z_z: np.ndarray
    z0: float
    omega_w: np.ndarray
    phi_qaz: np.ndarray

    @classmethod
    def from_file(cls, name: Union[Path, str]):
        data = read(name)
        if 'q_abs' in data:
            return cls._from_old_file(data)
        try:
            q_q = data['q_q']
            chi_qwab = data['chi_qwab']
            drho_qza = data['drho_qza']
            z_z = data['z_z']
            z0 = data['z0']
            omega_w = data['omega_w']
            phi_qaz = data['phi_qaz']
        except KeyError as err:
            if 'chi_qwgg' in data:
                raise KeyError(
                    'You might be trying to load a building block from a '
                    'ChiFile. You first have to construct the building block '
                    'file using qeh.bb_calculator.bb_builder.inter'
                    'polate_chi_to_bb')
            else:
                raise err
        return cls(q_q=q_q,
                   chi_qwab=chi_qwab,
                   drho_qza=drho_qza,
                   z_z=z_z,
                   z0=z0,
                   omega_w=omega_w,
                   phi_qaz=phi_qaz)

    @classmethod
    def _from_old_file(cls, data: dict):
        q_q = data['q_abs']
        omega_w = data['omega_w']
        z_z = data['z']
        dZ = z_z[1] - z_z[0]
        Z0 = (z_z.max() + dZ) / 2
        Z_Z = z_z - Z0

        chim_qw = data['chiM_qw']
        chid_qw = data['chiD_qw']
        if 'chiDM_qw' in data:
            chidm_qw = data['chiDM_qw']
        else:
            chidm_qw = np.zeros(chim_qw.shape)
        if 'chiMD_qw' in data:
            chimd_qw = data['chiMD_qw']
        else:
            chimd_qw = np.zeros(chim_qw.shape)

        # un-normalize densities
        drhom_qZ = data['drhoM_qz'] * chim_qw[:, 0, np.newaxis]
        drhod_qZ = data['drhoD_qz'] * chid_qw[:, 0, np.newaxis]

        drho_qZa = np.zeros((len(q_q), len(Z_Z), 2), dtype=complex)
        drho_qZa[:, :, 0] = drhom_qZ
        drho_qZa[:, :, 1] = drhod_qZ

        chiold_qwab = np.zeros((len(q_q), len(omega_w), 2, 2), dtype=complex)
        chiold_qwab[:, :, 0, 0] = chim_qw
        chiold_qwab[:, :, 0, 1] = chimd_qw
        chiold_qwab[:, :, 1, 0] = chidm_qw
        chiold_qwab[:, :, 1, 1] = chid_qw

        g_qab = np.zeros((len(q_q), 2, 2), dtype=complex)
        g_qab[:, 0, 0] = np.sum(drhom_qZ, axis=1) * dZ
        g_qab[:, 0, 1] = np.sum(drhod_qZ, axis=1) * dZ
        g_qab[:, 1, 0] = np.sum(drhom_qZ * Z_Z, axis=1) * dZ
        g_qab[:, 1, 1] = np.sum(drhod_qZ * Z_Z, axis=1) * dZ
        ginv_qwab = np.linalg.inv(g_qab)[:, None, :, :]

        grho_qab = np.zeros((len(q_q), 2, 2), dtype=complex)
        grho_qab[:, 0, 0] = np.sum(np.abs(drhom_qZ)**2, axis=1) * dZ
        grho_qab[:, 0, 1] = np.sum(drhom_qZ.conj() * drhod_qZ, axis=1) * dZ
        grho_qab[:, 1, 0] = np.sum(drhod_qZ.conj() * drhom_qZ, axis=1) * dZ
        grho_qab[:, 1, 1] = np.sum(np.abs(drhod_qZ)**2, axis=1) * dZ
        grho_qwab = grho_qab[:, None, :, :]

        chi_qwab = grho_qwab @ ginv_qwab @ chiold_qwab
        phi_qaz = Z_Z[None, None, :]**np.arange(2)[None, :, None] * \
            np.ones_like(q_q)[:, None, None]

        # Renormalize densities
        norm_drho_qa = np.sqrt(trapezoid(
            np.abs(drho_qZa)**2, axis=1, dx=dZ))
        drho_qZa = drho_qZa / norm_drho_qa[:, None, :]
        chi_qwab /= norm_drho_qa[:, None, :, None]

        return cls(q_q=q_q,
                   chi_qwab=chi_qwab,
                   drho_qza=drho_qZa,
                   z_z=z_z,
                   z0=Z0,
                   omega_w=omega_w,
                   phi_qaz=phi_qaz)


class QInterpolator:
    def __init__(self,
                 data_qX: np.ndarray,
                 q_q: np.ndarray,
                 ddata_X: Union[np.ndarray, None] = None,
                 dddata_X: Union[np.ndarray, None] = None):

        if (ddata_X is not None) and (dddata_X is not None):
            raise NotImplementedError('Both derivatives, \
                                      ddata_X and dddata_X, \
                                      must not be specified.')

        if (ddata_X is None) and (dddata_X is None):
            BC = 'not-a-knot'
        else:
            if ddata_X is not None:
                derivatives = (1, ddata_X)
            if dddata_X is not None:
                derivatives = (2, dddata_X)
            BC = (derivatives, 'not-a-knot')

        self.interp = CubicSpline(x=q_q,
                                  y=data_qX,
                                  axis=0,
                                  bc_type=BC)

    def __call__(self, q_Q):
        return self.interp(q_Q)


class BB_QWInterpolator:
    def __init__(self,
                 bb: BBData):
        self.q_q = bb.q_q
        self.chi_qwab = bb.chi_qwab
        self.drho_qza = bb.drho_qza
        self.phi_qaz = bb.phi_qaz
        self.omega_w = bb.omega_w
        self.z_z = bb.z_z
        self.z0 = bb.z0

        self.chiXX_Qwxx = QInterpolator(self.chi_qwab,
                                        self.q_q)

        self.drhoX_Qzx = QInterpolator(self.drho_qza,
                                       self.q_q)
        
        self.phi_Qaz = QInterpolator(self.phi_qaz,
                                     self.q_q)

    def __call__(self, q_Q, w_W):
        drho_Qza = self.drhoX_Qzx(q_Q)
        chi_Qwab = self.chiXX_Qwxx(q_Q)
        phi_Qaz = self.phi_Qaz(q_Q)

        if not (w_W is self.omega_w):
            chi_QWab = CubicSpline(self.omega_w, chi_Qwab, axis=1)(w_W)
        else:
            chi_QWab = chi_Qwab

        return drho_Qza, chi_QWab, phi_Qaz


def interpolate_building_blocks(BBfiles: list[str],
                                q_grid: Union[np.ndarray, None] = None,
                                w_grid: Union[np.ndarray, None] = None):
    """ Interpolate building blocks to same frequency-
    and q- grid. The interpolated files will be saved
    in the same folder as the original files.

    Parameters
    ----------
    BBfiles: list of str
        list of names of the building block files to be interpolated
    q_grid: np.ndarray | None
        the q-grid [unit: 1/Bohr]. If None, it will be loaded from the
        first BBfile.
    w_grid: np.ndarray | None
        the frequency grid [unit: Hartree]. If None, it will be loaded
        from the first BBfile.

    Raises
    ------
    ValueError
        if max(q_grid) > q_max of any of the BBfiles
        if max(w_grid) > w_max of any of the BBfiles

    Returns
    -------
    None
    """
    assert world.rank == 0

    bb_n = []
    for il, filename in enumerate(BBfiles):
        bb_n.append(BBData.from_file(filename))

    # Find q_max and w_max and their indicies
    q_max = np.inf
    qarg_max = 0
    w_max = np.inf
    warg_max = 0
    for il, bb in enumerate(bb_n):
        if q_max > np.max(bb.q_q):
            qarg_max = il
            q_max = np.max(bb.q_q)
        if w_max > np.max(bb.omega_w):
            warg_max = il
            w_max = np.max(bb.omega_w)

    # Set q_grid and w_grid
    if q_grid is None:
        q_grid = bb_n[qarg_max].q_q
    else:
        if np.max(q_grid) > q_max:
            raise ValueError('Max of q_grid is larger than the q_max \
                             in the BB files')
    if w_grid is None:
        w_grid = bb_n[warg_max].omega_w
    else:
        if np.max(w_grid) > w_max:
            raise ValueError('Max of w_grid is larger than the w_max \
                             in the BB files')

    # Interpolate to same grid specified by q_grid and w_grid
    for bb, filename in zip(bb_n, BBfiles):
        name = Path(filename).stem
        newname = Path(filename).with_stem(f'{name}-int')
        q_interp = BB_QWInterpolator(bb)
        drho_Qza, chi_QWab, phi_Qaz = q_interp(q_grid, w_grid)
        data = {'q_q': q_grid,
                'omega_w': w_grid,
                'chi_qwab': chi_QWab,
                'drho_qza': drho_Qza,
                'z0': bb.z0,
                'z_z': q_interp.z_z,
                'phi_qaz': phi_Qaz}

        write(newname, data)
