
from dataclasses import dataclass
from typing import Union
import numpy as np
if np.__version__ < '2.0.0':
    from numpy import trapz as trapezoid
else:
    from numpy import trapezoid
from scipy.interpolate import CubicSpline
from ase.parallel import world
from pathlib import Path

from qeh.bb_calculator.bb_interpolator import QInterpolator
from qeh.tools.io import read, write
from qeh.bb_calculator.basis_functions import PotentialBasis, PolynomialBasis


@dataclass
class ChiFile:
    q_q: np.ndarray
    omega_w: np.ndarray
    Gz_g: np.ndarray
    chi_qwgg: np.ndarray
    L: float

    @classmethod
    def from_file(cls, name: Union[Path, str], format=None):
        data = read(name, format)
        try:
            q_q = data['q_q']
            omega_w = data['omega_w']
            Gz_g = data['Gz_g']
            chi_qwgg = data['chi_qwgg']
            L = data['L']
        except KeyError as err:
            if 'chi_qwab' in data:
                raise KeyError(
                    'You might be trying to read a building-block file. '
                    'To do so, use the BuildingBlock class.')
            else:
                raise err
        return cls(q_q=q_q,
                   omega_w=omega_w,
                   Gz_g=Gz_g,
                   chi_qwgg=chi_qwgg,
                   L=L)


def get_Gz_mask(chifile: ChiFile, zN: int):
    i_Qc = np.arange(zN)
    i_Qc += zN // 2
    i_Qc %= zN
    i_Qc -= zN // 2
    B_cv = 2.0 * np.pi / chifile.L
    G_Qv = i_Qc * B_cv
    GzIndices = np.zeros(len(chifile.Gz_g), dtype=int)
    for g, Gz in enumerate(chifile.Gz_g):
        GzIndices[g] = np.where(np.isclose(G_Qv, Gz))[0][0]
    assert len(GzIndices) == len(chifile.Gz_g)
    assert np.allclose(G_Qv[GzIndices], chifile.Gz_g)
    return GzIndices


class BBBuilder:
    def __init__(self, bb: ChiFile, zN: int, aN: int, ifft_phi: bool,
                 basis: PotentialBasis = PolynomialBasis()):
        self.bb = bb
        self.zN = zN
        self.aN = aN
        self.ifft_phi = ifft_phi
        self.z_z = np.linspace(0, self.bb.L, zN, endpoint=False)
        self.z0 = self.bb.L / 2  # XXX: Scary assumption
        self.basis = basis
        self.GzIndices = get_Gz_mask(self.bb, zN)

        # Known derivates y = ax**2 -> y' = 2 y / x
        dchiMM_w = bb.chi_qwgg[0, :, 0, 0] / bb.q_q[0] * 2
        # Known derivates y = ax -> y' = y / x
        dchiMX_w = bb.chi_qwgg[0, :, 0, 1:] / bb.q_q[0]
        dchiXM_w = bb.chi_qwgg[0, :, 1:, 0] / bb.q_q[0]

        self.chiMM_Qw = QInterpolator(bb.chi_qwgg[:, :, 0, 0],
                                      bb.q_q,
                                      ddata_X=dchiMM_w)

        self.chiMX_Qwx = QInterpolator(bb.chi_qwgg[:, :, 0, 1:],
                                       bb.q_q,
                                       ddata_X=dchiMX_w)

        self.chiXM_Qwx = QInterpolator(bb.chi_qwgg[:, :, 1:, 0],
                                       bb.q_q,
                                       ddata_X=dchiXM_w)

        self.chiXX_Qwxx = QInterpolator(bb.chi_qwgg[:, :, 1:, 1:],
                                        bb.q_q)

    def __call__(self, q_Q, w_W=None):
        wN = len(self.bb.omega_w) if w_W is None else len(w_W)
        chi_QWab = np.empty_like(self.bb.chi_qwgg,
                                 shape=(len(q_Q), wN, self.aN, self.aN))
        drho_Qza = np.empty_like(self.bb.chi_qwgg,
                                 shape=(len(q_Q), self.zN, self.aN))
        L = self.bb.L
        zG_zg = np.exp(1j * np.outer(self.z_z, self.bb.Gz_g))
        z_z = self.z_z

        # Basis:
        phi_Qaz = self.basis(q_Q, self.aN, z_z - self.z0)

        # FFT is king:
        zfactor_Qag = \
            np.fft.fft(phi_Qaz, axis=2, norm='forward')[..., self.GzIndices]

        if self.ifft_phi:
            phi_QaG = np.zeros_like(phi_Qaz, dtype=complex)
            phi_QaG[..., self.GzIndices] = zfactor_Qag
            phi_Qaz = np.fft.ifft(phi_QaG, axis=2, norm='forward')

        for Q, q in enumerate(q_Q):
            chi_wgg = self.interpQ(q)
            if w_W is None:
                chi_Wgg = chi_wgg
            else:
                chi_Wgg = CubicSpline(self.bb.omega_w, chi_wgg)(w_W)

            # Calculate the building block
            drho_Wga = chi_Wgg @ zfactor_Qag[Q].T  # induced densities
            drho_Qza[Q] = zG_zg @ drho_Wga[0]  # density basis functions
            chi_QWab[Q] = L * drho_Wga[0].T.conj() @ drho_Wga

        # Normalize densities
        norm_drho_Qa = np.sqrt(trapezoid(
            np.abs(drho_Qza)**2, axis=1, x=z_z))
        drho_Qza = drho_Qza / norm_drho_Qa[:, None, :]
        chi_QWab /= norm_drho_Qa[:, None, :, None]

        return drho_Qza, chi_QWab, phi_Qaz

    def interpQ(self, q):
        wN = self.bb.chi_qwgg.shape[1]
        gn = self.bb.chi_qwgg.shape[2]
        chi_wgg = np.empty_like(self.bb.chi_qwgg, shape=(wN, gn, gn))
        chi_wgg[:, 0, 0] = self.chiMM_Qw(q)
        chi_wgg[:, 0, 1:] = self.chiMX_Qwx(q)
        chi_wgg[:, 1:, 0] = self.chiXM_Qwx(q)
        chi_wgg[:, 1:, 1:] = self.chiXX_Qwxx(q)
        return chi_wgg


def interpolate_chi_to_bb(file: Union[Path, str],
                          outfile: Union[str, None] = None,
                          zN: int = 200,
                          aN: int = 4,
                          q_grid: Union[np.ndarray, int, None] = None,
                          w_grid: Union[np.ndarray, int, None] = None,
                          ifft_phi: bool = False,
                          basis: PotentialBasis = PolynomialBasis()) -> None:
    """ Interpolate chi file and calculate the building block.
    The interpolated files will be saved
    in the same folder as the original files.

    Parameters
    ----------
    BBfiles: Path or str
        name of the building block file to be interpolated
    q_grid: np.ndarray | None
        the q-grid [unit: 1/Bohr]. If None, it will be loaded from the
        first BBfile.
    w_grid: np.ndarray | None
        the frequency grid [unit: Hartree]. If None, it will be loaded
        from the first BBfile.
    zN: int
        the number of z grid points used for interpolation in QEH and
        numerical integration. Default is 200.
    aN: int
        the number of basis functions. Default is 4.

    Raises
    ------
    ValueError
        if max(q_grid) > q_max of any of the BBfiles
        if max(w_grid) > w_max of any of the BBfiles

    Returns
    -------
    None
    """
    assert world.rank == 0

    bb = ChiFile.from_file(file)

    q_max = bb.q_q.max()
    q_min = bb.q_q.min()
    w_max = bb.omega_w.max()
    w_min = bb.omega_w.min()
    power = 4  # XXX: hard-coded and arbitrary

    if isinstance(q_grid, int):
        q_grid = np.linspace(q_min**(1 / power),
                             q_max**(1 / power),
                             q_grid)**power
    elif isinstance(q_grid, np.ndarray):
        if q_grid.max() > q_max:
            raise ValueError("Max of q_grid > q_max in BBfile")
    elif q_grid is None:
        q_grid = np.linspace(q_min**(1 / power),
                             q_max**(1 / power),
                             512)**power

    if isinstance(w_grid, int):
        w_grid = np.linspace(w_min, w_max, w_grid)
    elif isinstance(w_grid, np.ndarray):
        if w_grid.max() > w_max:
            raise ValueError("Max of w_grid > w_max in BBfile")

    bb_builder = BBBuilder(bb=bb, zN=zN, aN=aN,
                           ifft_phi=ifft_phi, basis=basis)
    drho_Qza, chi_QWab, phi_Qaz = bb_builder(q_grid, w_grid)

    data = {'q_q': q_grid,
            'omega_w': bb.omega_w if w_grid is None else w_grid,
            'chi_qwab': chi_QWab,
            'drho_qza': drho_Qza,
            'z0': bb_builder.z0,
            'z_z': bb_builder.z_z,
            'phi_qaz': phi_Qaz}
    if outfile is None:
        name = Path(file).stem
        if name[-4:] == '-chi':
            name = name[:-4]
        newname = name + '-bb'
        outfile = Path(file).with_stem(newname)
    write(outfile, data)
