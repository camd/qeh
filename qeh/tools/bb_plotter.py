import matplotlib.pyplot as plt
import numpy as np
from ase.units import Bohr

from qeh.bb_calculator.bb_interpolator import BB_QWInterpolator, BBData


def plot_bb(bbname: str,
            outputpath: str | None = None,
            q_q: np.ndarray | None = None):

    if q_q is None:
        q_grid = np.linspace(0, 2 * Bohr, 1000)
    else:
        q_grid = q_q

    if outputpath is None:
        outputpath = './'

    if bbname[-7:] != '-bb.npz':
        bbname += '-bb.npz'
    sbb = BBData.from_file(bbname)

    interp = BB_QWInterpolator(sbb)

    drho_qza, chi_qwab = interp(q_grid)

    plt.figure(figsize=(19.2, 10.8))
    plt.plot(q_grid, np.real(chi_qwab[:, 0, 0, 0]), linewidth=2)
    plt.plot(sbb.q_q, np.real(sbb.chi_qwab[:, 0, 0, 0]), '.', markersize=3)
    plt.plot(q_grid, np.imag(chi_qwab[:, 0, 0, 0]), linewidth=2)
    plt.plot(sbb.q_q, np.imag(sbb.chi_qwab[:, 0, 0, 0]), '.', markersize=3)
    plt.xlim(q_grid[0], q_grid[-1])
    ymax = max(np.real(chi_qwab[:, 0, 0, 0])) \
        + max(np.abs(chi_qwab[:, 0, 0, 0])) * 0.1
    ymin = min(np.real(chi_qwab[:, 0, 0, 0])) \
        - max(np.abs(chi_qwab[:, 0, 0, 0])) * 0.1
    dy = (ymax - ymin) * 0.1
    plt.ylim(ymin - dy, ymax + dy)
    plt.savefig(outputpath + 'chiMM.png')

    plt.figure(figsize=(19.2, 10.8))
    plt.plot(q_grid, np.real(chi_qwab[:, 0, 1, 0]), linewidth=2)
    plt.plot(sbb.q_q, np.real(sbb.chi_qwab[:, 0, 1, 0]), '.', markersize=3)
    plt.plot(q_grid, np.imag(chi_qwab[:, 0, 1, 0]), linewidth=2)
    plt.plot(sbb.q_q, np.imag(sbb.chi_qwab[:, 0, 1, 0]), '.', markersize=3)
    plt.xlim(q_grid[0], q_grid[-1])
    plt.savefig(outputpath + 'chiDM.png')

    plt.figure(figsize=(19.2, 10.8))
    plt.plot(q_grid, np.real(chi_qwab[:, 0, 0, 1]), linewidth=2)
    plt.plot(sbb.q_q, np.real(sbb.chi_qwab[:, 0, 0, 1]), '.', markersize=3)
    plt.plot(q_grid, np.imag(chi_qwab[:, 0, 0, 1]), linewidth=2)
    plt.plot(sbb.q_q, np.imag(sbb.chi_qwab[:, 0, 0, 1]), '.', markersize=3)
    plt.xlim(q_grid[0], q_grid[-1])
    plt.savefig(outputpath + 'chiMD.png')

    plt.figure(figsize=(19.2, 10.8))
    plt.plot(q_grid, np.real(chi_qwab[:, 0, 1, 1]), linewidth=2)
    plt.plot(sbb.q_q, np.real(sbb.chi_qwab[:, 0, 1, 1]), '.', markersize=3)
    plt.plot(q_grid, np.imag(chi_qwab[:, 0, 1, 1]), linewidth=2)
    plt.plot(sbb.q_q, np.imag(sbb.chi_qwab[:, 0, 1, 1]), '.', markersize=3)
    plt.xlim(q_grid[0], q_grid[-1])
    ymax = max(np.real(chi_qwab[:, 0, 1, 1]))
    ymin = min(np.real(chi_qwab[:, 0, 1, 1]))
    dy = (ymax - ymin) * 0.1
    plt.ylim(ymin - dy, ymax + dy)
    plt.savefig(outputpath + 'chiDD.png')

    plt.figure(figsize=(19.2, 10.8))
    absmaxind = np.argmax(np.abs(drho_qza[:, :, 0]), axis=1)
    sabsmaxind = np.argmax(np.abs(sbb.drho_qza[:, :, 0]), axis=1)
    dmrho_qza = drho_qza[np.arange(len(absmaxind)), absmaxind, 0]
    sdmrho_qza = sbb.drho_qza[np.arange(len(sabsmaxind)), sabsmaxind, 0]
    plt.plot(q_grid, dmrho_qza.real, linewidth=2)
    plt.plot(sbb.q_q, sdmrho_qza.real, '.', markersize=3)
    plt.plot(q_grid, dmrho_qza.imag, linewidth=2)
    plt.plot(sbb.q_q, sdmrho_qza.imag, '.', markersize=3)
    plt.xlim(q_grid[0], q_grid[-1])
    ymax = np.max(np.real(drho_qza[:, :, 0]))
    ymin = np.min(np.real(drho_qza[:, :, 0]))
    dy = (ymax - ymin) * 0.1
    plt.ylim(ymin - dy, ymax + dy)
    plt.savefig(outputpath + 'AdrhoM.png')

    plt.figure(figsize=(19.2, 10.8))
    plt.plot(sbb.z_z, np.real(drho_qza[:10, :, 0].T), linewidth=2)
    plt.savefig(outputpath + 'RdrhoM.png')

    plt.figure(figsize=(19.2, 10.8))
    plt.plot(sbb.z_z, np.real(drho_qza[:10, :, 1].T), linewidth=2)
    plt.savefig(outputpath + 'RdrhoD.png')

    plt.figure(figsize=(19.2, 10.8))
    plt.plot(sbb.z_z, np.imag(drho_qza[:10, :, 0].T), linewidth=2)
    plt.savefig(outputpath + 'IdrhoM.png')

    plt.figure(figsize=(19.2, 10.8))
    plt.plot(sbb.z_z, np.imag(drho_qza[:10, :, 1].T), linewidth=2)
    plt.savefig(outputpath + 'IdrhoD.png')
