import h5py
import numpy as np
from qeh.tools.io import FileHandler
from ase.parallel import world


class HDF5Handler(FileHandler):

    @staticmethod
    def read(filename):
        def recursively_load(hdf_group):
            data = {}
            for key, item in hdf_group.items():
                if isinstance(item, h5py.Dataset):
                    # Check if it's a string and decode it if necessary
                    if item.dtype.kind == 'S':  # Byte string
                        data[key] = item[()].decode('utf-8')
                    else:
                        data[key] = item[()]
                        # If scalar, convert from array to standard Python type
                        if np.isscalar(data[key]):
                            data[key] = data[key].item()
                elif isinstance(item, h5py.Group):
                    data[key] = recursively_load(item)
                else:
                    data[key] = item
            return data

        with h5py.File(filename, 'r') as hdf_file:
            return recursively_load(hdf_file)

    @staticmethod
    def write(filename, data):
        if world.rank == 0:
            def recursively_save(hdf_group, data_dict):
                for key, item in data_dict.items():
                    if isinstance(item, dict):
                        subgroup = hdf_group.create_group(key)
                        recursively_save(subgroup, item)
                    elif isinstance(item, str):
                        # Store strings as UTF-8 encoded byte strings
                        hdf_group.create_dataset(key, data=np.bytes_(item))
                    else:
                        hdf_group.create_dataset(key, data=item)

            with h5py.File(filename, 'w') as hdf_file:
                recursively_save(hdf_file, data)
        else:
            pass
