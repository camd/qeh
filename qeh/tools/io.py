from abc import ABC, abstractmethod
from pathlib import Path
_supported_types = {'npz', 'json', 'hdf5', 'h5'}
_default = 'npz'


def read(filename, format=None):
    path, format = parse_filename_and_format(filename, format, mode='r')
    handler = get_file_handler(format)
    data = handler.read(path)
    return data


def write(filename, data, format=None):
    path, format = parse_filename_and_format(filename, format, mode='w')
    handler = get_file_handler(format)
    return handler.write(path, data)


def parse_filename_and_format(filename, format, mode='r'):
    path = Path(filename)
    if not path.suffix:  # add file extension to path
        if format is not None:
            path = path.with_suffix(f'.{format}')
            return path, format
        elif mode == 'r':  # check for existing files to guess format
            for suffix in _supported_types:
                newpath = path.with_suffix(f'.{suffix}')
                if newpath.is_file():
                    path = newpath
                    format = suffix
                    return path, format
            raise FileNotFoundError(f'{path} not found!')
        elif mode == 'w':
            path = path.with_suffix(f'.{_default}')
            format = _default
            return path, format

    if format is None:
        format = path.suffix[1:]  # remove leading dot from file extension
    if format not in _supported_types:
        raise ValueError('Unsupported file type. '
                         f'Supported types: {_supported_types}')
    return path, format


def get_file_handler(format: str):
    _format = format.lower()
    if _format not in _supported_types:
        raise ValueError(f'_Format {format} is not supported')
    if _format in {'hdf5', 'h5'}:
        from qeh.tools.hdf5 import HDF5Handler
        return HDF5Handler()
    if _format == 'npz':
        from qeh.tools.npz import NpzHandler
        return NpzHandler()
    if _format == 'json':
        from qeh.tools.jsonio import JSONHandler
        return JSONHandler()


class FileHandler(ABC):

    @staticmethod
    @abstractmethod
    def read(filename, format):
        raise NotImplementedError()

    @staticmethod
    @abstractmethod
    def write(filename, data, format):
        raise NotImplementedError()
