from scipy.interpolate import CubicSpline
import numpy as np
from scipy.special import jv, j0, j1, struve


def integrate_j0_spline(spl: CubicSpline, r_r, integrate_from_zero=True):
    """
    Computes the definite integral of f(q)*j0(r*q) from a to b,
    where f(q) is represented by a cubic spline, and a, b are respectively
    the first and last sample points in the spline.

    Brief description of the method:
    The spline describes the function of interest f(q) in terms of piecewise
    cubic polynomials. The antiderivatives of the functions x^n j0(x) for
    n = 0, 1, 2, 3 can be expressed analytically in terms of bessel and struve
    functions. This is used to analytically evaluate the integral on each
    piecewise interval. At the end, the definite integral over each piecewise
    section is summed to obtain the full definite integral.

    Notes:

    - c_nq are spline expansion coefficients in terms of the functions
      (q - q_i)^n. With the variable coef_nq, these are re-mapped to expansion
      coefficients in terms of q^n.

    - for best numerical stability, the endpoints are handled separately.
      This is done to avoid the subtraction numbers that are very close as
      much as possible, as this operation reduces numerical precision.

    """
    r_rx = r_r.reshape(-1, 1)
    q_q = spl.x
    if integrate_from_zero:
        q_q[0] = 0
    c_nq = spl.c
    qr_rq = q_q * r_rx
    bessel0_rq = j0(qr_rq)
    bessel1_rq = j1(qr_rq)
    bessel2_rq = jv(2, qr_rq)
    struve0_rq = struve(0, qr_rq)
    struve1_rq = struve(1, qr_rq)
    i_rnq = np.zeros((r_rx.size, 4, q_q.size))

    i_rnq[:, 0] = _intj0(q_q, bessel0_rq, bessel1_rq,
                         struve0_rq, struve1_rq)
    i_rnq[:, 1] = _intxj0(q_q, r_rx, bessel1_rq)
    i_rnq[:, 2] = _intx2j0(q_q, r_rx, bessel0_rq, bessel1_rq,
                           struve0_rq, struve1_rq)
    i_rnq[:, 3] = _intx3j0(q_q, r_rx, bessel1_rq, bessel2_rq)

    qa_q = q_q[:-1]
    coef_nq = np.zeros_like(c_nq)
    coef_nq[0] = c_nq[3] - c_nq[2] * qa_q + \
        c_nq[1] * qa_q**2 - c_nq[0] * qa_q**3
    coef_nq[1] = c_nq[2] - 2 * c_nq[1] * qa_q + 3 * c_nq[0] * qa_q**2
    coef_nq[2] = c_nq[1] - 3 * c_nq[0] * qa_q
    coef_nq[3] = c_nq[0]
    endpoints_r = (coef_nq[:, -1] * i_rnq[..., -1]
                   - i_rnq[..., 0] * coef_nq[:, 0]).sum(1)
    coef_nq = -np.diff(coef_nq, axis=1)
    res_r = endpoints_r + (coef_nq * i_rnq[..., 1:-1]).sum(1).sum(1)
    return res_r


def _intj0(x, bessel0, bessel1, struve0, struve1):
    """computes antiderivative of j0(x) wrt x"""
    i = np.pi / 2 * x * struve0 * bessel1 \
        + 1 / 2 * x * (2 - np.pi * struve1) * bessel0
    return i


def _intxj0(x, r, bessel1):
    """computes antiderivative of x * j0(x * r) wrt x"""
    return x * bessel1 / r


def _intx2j0(x, r, bessel0, bessel1, struve0, struve1):
    """computes antiderivative of x**2 * j0(x * r) wrt x"""
    y = x * r
    i = 1 / 2 * bessel1 * (2 * y**2 - np.pi * y * struve0) \
        + 1 / 2 * np.pi * y * bessel0 * struve1
    return i / r**3


def _intx3j0(x, r, bessel1, bessel2):
    """computes antiderivative of x**3 * j0(x * r) wrt x"""
    i = x**3 / r * bessel1 - 2 * (x / r)**2 * bessel2
    return i
