import numpy as np
from qeh.tools.io import FileHandler


class NpzHandler(FileHandler):
    """
    Wrapper class for reading and writing npz files,
    supporting nested dictionaries
    """

    @staticmethod
    def read(filename):
        data = _unflatten_dict(flat_data=np.load(filename))
        return data

    @staticmethod
    def write(filename, data: dict):
        dct = _flatten_dict(data)
        np.savez_compressed(filename, **dct)


def _flatten_dict(data: dict, subdict_name=''):
    flat_data = {}
    for key, value in data.items():
        if isinstance(value, dict):
            subdct = _flatten_dict(data=value,
                                   subdict_name=f'{subdict_name}{key}__.')
            flat_data.update(subdct)
        else:
            flat_data[f'{subdict_name}{key}'] = value
    return flat_data


def _unflatten_dict(flat_data, delimiter="__."):
    def insert_nested_key(nested_dict, keys, value):
        current = nested_dict
        for key in keys[:-1]:
            # Traverse the path, creating nested dictionaries as needed
            if key not in current:
                current[key] = {}
            current = current[key]
        current[keys[-1]] = value

    nested_dict = {}
    for flat_key, value in flat_data.items():
        # Split each flat key into a list of keys for nested dictionaries
        keys = flat_key.split(delimiter)
        insert_nested_key(nested_dict, keys, value)
    return nested_dict
