import json
import numpy as np
from qeh.tools.io import FileHandler
from ase.parallel import paropen


class JSONHandler(FileHandler):
    """
    Wrapper class for reading and writing json files
    """

    @staticmethod
    def read(filename):
        encoder = NumpyEncoder()
        data = encoder.load(filename)
        return data

    @staticmethod
    def write(filename, data: dict):
        encoder = NumpyEncoder()
        encoder.dump(data, filename)


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            # Check if the array is of complex type
            if np.iscomplexobj(obj):
                return {
                    '__complex_ndarray__': True,
                    'real': obj.real.tolist(),
                    'imag': obj.imag.tolist(),
                    'dtype': str(obj.dtype),
                    'shape': obj.shape
                }
            else:
                return {
                    '__ndarray__': obj.tolist(),
                    'dtype': str(obj.dtype),
                    'shape': obj.shape
                }
        elif isinstance(obj, complex):  # For individual complex numbers
            return {'__complex__': True, 'real': obj.real, 'imag': obj.imag}
        elif isinstance(obj, np.int64):
            return int(obj)
        return super().default(obj)

    @staticmethod
    def decode(dct):
        """
        Static method to decode JSON objects into numpy arrays or complex
        numbers when needed.
        """
        if '__complex_ndarray__' in dct:
            real = np.array(dct['real'], dtype=dct['dtype'])
            imag = np.array(dct['imag'], dtype=dct['dtype'])
            return real + 1j * imag
        elif '__ndarray__' in dct:
            return np.array(
                dct['__ndarray__'], dtype=dct['dtype']).reshape(dct['shape'])
        elif '__complex__' in dct:
            return complex(dct['real'], dct['imag'])
        return dct

    @classmethod
    def dump(cls, data, filename, **kwargs):
        """Class method to encode and save data to a JSON file."""
        with paropen(filename, 'w') as f:
            json.dump(data, f, cls=cls, **kwargs)

    @classmethod
    def load(cls, filename, **kwargs):
        """Class method to load and decode data from a JSON file."""
        with paropen(filename, 'r') as f:
            return json.load(f, object_hook=cls.decode, **kwargs)
