from __future__ import annotations
import numpy as np
from scipy.interpolate import CubicSpline
from ase.utils.timing import timer, Timer
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from qeh.heterostructure import Heterostructure


class Substrate:
    def __init__(self,
                 eps_W: np.ndarray,
                 omega_W: np.ndarray,
                 dist: float,
                 isotropic: bool = True,
                 timer: Timer | None = None):
        """
        Class for representing substrates
        eps_W: Dielectric function of the substrate (frequency dependent)
            (epsx and epsz in the anisotropic case)
        omega_W: Fequency grid of the eps [unit: Hartree]
        d: Distance of the substrate to the middle of the
            first layer [unit: Bohr]
        isotropic: includes the out-of-plane dielectric function in
            the substrate if False
        """

        self.eps_W = eps_W
        self.omega_W = omega_W
        if len(omega_W) == 1:
            self.static = True
        else:
            self.static = False
        self.dist = dist
        self.isotropic = isotropic
        if not isotropic:
            raise NotImplementedError()
        self.timer = Timer() if timer is None else timer

    def interpolate_eps_to_frequency_grid(self, w_w) -> np.ndarray:
        eps_spline = CubicSpline(self.omega_W, self.eps_W)
        eps_w = eps_spline(w_w)
        return eps_w

    def get_unscreened_image_potential(self, dphi_qzi, z_z, q_q, z0):
        """
        Get potential from image charges on the heterostructure z-grid
        The image potential of from density basis function 'i' is related
        to the 'direct' potential of basis function 'i' by the coordinate
        transformation z => -z + 2(z0 - d).
        z0 is the center of the first heterostructure layer, and d is the
        distance between the center of this layer  and the surface of the
        substrate.
        """

        # find grid point nearest substrate
        iz_substrate = np.argmin(np.abs(z_z - (z0 - self.dist)))

        # distance from substrate to grid point nearest substrate
        z_offset = z_z[iz_substrate] - (z0 - self.dist)

        # find induced potential at substrate surface
        # This is found from the potential at the grid point nearest the
        # surface, multiplied by an analytical correction, accounting for
        # whatever off-set there might be between the grid point
        # and the actual location of the surface
        dphi_qi = dphi_qzi[:, iz_substrate] * \
            np.exp(-z_offset * q_q[:, None])

        dphi_image_qzi = \
            - dphi_qi[:, None, :] * \
            np.exp(-np.abs(-z_z[:, None] + z0 - self.dist)
                   * q_q[:, None, None])
        dphi_image_qzi[:, z_z < z0 - self.dist] = 0
        return dphi_image_qzi

    @timer('Get substrate coulomb kernel')
    def get_Coulomb_kernel(self, hs: Heterostructure) -> np.ndarray:
        # Calculates potential due to image charges induced in the substrate
        qN, _, iN = hs.dphi_qzi.shape
        z_z = hs.z_z
        q_q = hs.q_q
        z0 = hs.layers_l[0].z0
        # image potential is a mirror image of ordinary induced potential
        # obtained from the coordinate transformation z -> -(z + 2d).
        # To avoid having large matrices of shape (q,w,z,i), we first calculate
        # the image charge without frequency-dependent screening, and then
        # account for the screening in the end
        dphi_image_qzi = self.get_unscreened_image_potential(
            hs.dphi_qzi, z_z, q_q, z0=z0)

        unscreened_kernel_qij = hs.phi_qiz @ dphi_image_qzi * hs.dz

        if self.static:
            screening_w = (self.eps_W - 1) / (self.eps_W + 1)
        else:
            omega_w = hs.omega_w
            eps_w = self.interpolate_eps_to_frequency_grid(omega_w)
            screening_w = (eps_w - 1) / (eps_w + 1)

        kernel_qwij = screening_w[:, None, None] * \
            unscreened_kernel_qij[:, None]
        return kernel_qwij

    def get_static_image_potential(self, dphi_qzi, z_z, q_q, z0) -> np.ndarray:
        # gets screened image potential in the static limit.
        # This function is used when calculating the exciton screened
        # interaction
        dphi_image_qzi = self.get_unscreened_image_potential(
            dphi_qzi, z_z, q_q, z0)
        if self.static:
            assert np.isclose(self.omega_W, 0)
            eps_static = self.eps_W
        else:
            eps_static = self.interpolate_eps_to_frequency_grid(w_w=[0])
        return dphi_image_qzi * (eps_static - 1) / (eps_static + 1)
