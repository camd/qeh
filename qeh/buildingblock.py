import numpy as np
from typing import Union
if np.__version__ < '2.0.0':
    from numpy import trapz as trapezoid
else:
    from numpy import trapezoid

from pathlib import Path
from functools import cached_property
from ase.utils.timing import timer, Timer
from scipy.interpolate import CubicSpline
from qeh.tools.io import read, write as write_file
from qeh.qwcomm import QComm


class BuildingBlock:
    def __init__(self,
                 q_Q: np.ndarray,
                 omega_w: np.ndarray,
                 Z_Z: np.ndarray,
                 drho_QZa: np.ndarray,
                 chi_Qwab: np.ndarray,
                 phi_qaZ: np.ndarray,
                 qcomm: Union[QComm, None] = None,
                 qmax: Union[float, None] = None,
                 wmax: Union[float, None] = None,
                 timer: Union[Timer, None] = None):
        '''
        Initialize the building block.

        Parameters
        ----------
        q_Q: np.ndarray
            the full BB q-grid
        omega_w: np.ndarray
            the full BB frequency-grid
        Z_Z: np.ndarray
            the building block z-grid
        drho_QZa: np.ndarray
            the building block induced densities
        chi_Qwab: np.ndarray
            the building block chi matrix
        qmax: float | None
            maximum q value
        wmax: float | None
            maximum frequency value
        timer: Timer | None
            timer

        Attributes
        ----------
        q_q: np.ndarray
            the distributed q-grid
        omega_w: np.ndarray
            the full BB frequency-grid
        Z_Z: np.ndarray
            the building block z-grid
        drho_qZa: np.ndarray
            the distributed building block induced densities
        chi_qwab: np.ndarray
            the distributed building block chi matrix
        drho_spline_qxa: CubicSpline
            the building block induced density spline
        dphi_qZa: np.ndarray
            the building block induced potential
        dphi_spline_qxa: CubicSpline
            the building block induced potential spline

        Class responsibilities
        ----------------------
        - Calculations in terms of the BB pole exspansion basis.
        - Spline interpolation
        '''

        self.timer = Timer() if timer is None else timer
        if qmax is not None:
            qindex = np.argmin(abs(q_Q - qmax)) + 1
        else:
            qindex = None
        if wmax is not None:
            windex = np.argmin(abs(omega_w - wmax)) + 1
        else:
            windex = None

        q_Q = q_Q[:qindex]
        chi_Qwab = chi_Qwab[:qindex, :windex]
        drho_QZa = drho_QZa[:qindex]

        self.Z_Z = Z_Z
        self.phi_qaZ = phi_qaZ[:qindex]
        self.dZ = Z_Z[1] - Z_Z[0]
        self.aN = drho_QZa.shape[2]
        self.omega_w = omega_w[:windex]

        if qcomm is None:
            qcomm = QComm(QN=len(q_Q))
        self.qcomm = qcomm

        i_q = qcomm.my_q
        self.q_q = q_Q[i_q]
        self.chi_qwab = chi_Qwab[i_q]
        self.drho_qZa = drho_QZa[i_q]
        self.phi_qaZ = phi_qaZ[i_q]

        self.phi_spline_qaZ = CubicSpline(x=Z_Z, y=self.phi_qaZ, axis=2)

        self.drho_spline_qxa = CubicSpline(x=self.Z_Z,
                                           y=self.drho_qZa,
                                           axis=1)

        self.dphi_qZa, self.dphi_spline_qxa = self.get_induced_potential()

    @classmethod
    def from_file(cls,
                  name: Union[Path, str],
                  format: Union[str, None] = None,
                  qcomm: Union[QComm, None] = None,
                  qmax: Union[float, None] = None,
                  wmax: Union[float, None] = None,
                  amax: Union[int, None] = None,
                  timer: Union[Timer, None] = None):
        data = read(name, format)
        if 'q_abs' in data:
            return cls._from_old_file(data,
                                      qcomm,
                                      qmax,
                                      wmax,
                                      amax,
                                      timer)
        try:
            q_q = data['q_q']
            omega_w = data['omega_w']
            Z_Z = data['z_z']
            Z0 = data['z0']
            Z_Z -= Z0  # Center Z_Z so Z0 is zero.
            dZ = Z_Z[1] - Z_Z[0]
            drho_qZa = data['drho_qza'][:, :, :amax]
            chi_qwab = data['chi_qwab'][:, :, :amax, :amax]
        except KeyError as err:
            if 'chi_qwgg' in data:
                raise KeyError(
                    'You might be trying to load a building block from a '
                    'ChiFile. You first have to construct the building block '
                    'file using qeh.bb_calculator.bb_builder.inter'
                    'polate_chi_to_bb')
            else:
                raise err
        norm_drho_qa = np.sqrt(trapezoid(
            np.abs(drho_qZa)**2, axis=1, dx=dZ))

        # reshape arrays for subsequent multiplications
        drho_qZa /= norm_drho_qa[:, None, :]
        chi_qwab /= norm_drho_qa[:, None, :, None]

        phi_qaZ = data['phi_qaz'][:, :amax, :]

        return cls(q_Q=q_q,
                   omega_w=omega_w,
                   Z_Z=Z_Z,
                   drho_QZa=drho_qZa,
                   chi_Qwab=chi_qwab,
                   phi_qaZ=phi_qaZ,
                   qcomm=qcomm,
                   qmax=qmax,
                   wmax=wmax,
                   timer=timer)

    @classmethod
    def _from_old_file(cls,
                       data,
                       qcomm: Union[QComm, None] = None,
                       qmax: Union[float, None] = None,
                       wmax: Union[float, None] = None,
                       amax: Union[int, None] = None,
                       timer: Union[Timer, None] = None):

        if amax is not None:
            assert amax > 1, \
                "amax must be at least 2 for the old files"

        q_q = data['q_abs']
        omega_w = data['omega_w']
        Z_Z = data['z']
        dZ = Z_Z[1] - Z_Z[0]
        Z0 = (Z_Z.max() + dZ) / 2

        Z_Z -= Z0  # Center Z_Z so Z0 is zero.
        chim_qw = data['chiM_qw']
        chid_qw = data['chiD_qw']
        if 'chiDM_qw' in data:
            chidm_qw = data['chiDM_qw']
        else:
            chidm_qw = np.zeros(chim_qw.shape)
        if 'chiMD_qw' in data:
            chimd_qw = data['chiMD_qw']
        else:
            chimd_qw = np.zeros(chim_qw.shape)
        # Re-normalize densities
        drhom_qZ = data['drhoM_qz'] * chim_qw[:, 0, np.newaxis]
        drhod_qZ = data['drhoD_qz'] * chid_qw[:, 0, np.newaxis]
        drho_qZa = np.zeros((len(q_q), len(Z_Z), 2), dtype=complex)
        drho_qZa[:, :, 0] = drhom_qZ
        drho_qZa[:, :, 1] = drhod_qZ
        norm_drho_qa = np.sqrt(trapezoid(
            np.abs(drho_qZa)**2, axis=1, dx=dZ))
        drho_qZa = drho_qZa / norm_drho_qa[:, None, :]

        chiold_qwab = np.zeros((len(q_q), len(omega_w), 2, 2), dtype=complex)
        chi_qwab = np.zeros((len(q_q), len(omega_w), 2, 2), dtype=complex)
        chiold_qwab[:, :, 0, 0] = chim_qw
        chiold_qwab[:, :, 0, 1] = chimd_qw
        chiold_qwab[:, :, 1, 0] = chidm_qw
        chiold_qwab[:, :, 1, 1] = chid_qw

        g_qab = np.zeros((len(q_q), 2, 2), dtype=complex)
        g_qab[:, 0, 0] = np.sum(drhom_qZ, axis=1) * dZ
        g_qab[:, 0, 1] = np.sum(drhod_qZ, axis=1) * dZ
        g_qab[:, 1, 0] = np.sum(drhom_qZ * Z_Z, axis=1) * dZ
        g_qab[:, 1, 1] = np.sum(drhod_qZ * Z_Z, axis=1) * dZ
        ginv_qab = np.linalg.inv(g_qab)

        grho_qab = np.zeros((len(q_q), 2, 2), dtype=complex)
        grho_qab[:, 0, 0] = np.sum(np.abs(drhom_qZ)**2, axis=1) * dZ
        grho_qab[:, 0, 1] = np.sum(drhom_qZ.conj() * drhod_qZ, axis=1) * dZ
        grho_qab[:, 1, 0] = np.sum(drhod_qZ.conj() * drhom_qZ, axis=1) * dZ
        grho_qab[:, 1, 1] = np.sum(np.abs(drhod_qZ)**2, axis=1) * dZ
        for iq in range(len(q_q)):
            chi_qwab[iq] = grho_qab[iq] @ ginv_qab[iq] @ chiold_qwab[iq]

        chi_qwab /= norm_drho_qa[:, None, :, None]
        phi_qaZ = Z_Z[None, None, :]**np.arange(2)[None, :, None] * \
            np.ones_like(q_q)[:, None, None]

        return cls(q_Q=q_q,
                   omega_w=omega_w,
                   Z_Z=Z_Z,
                   drho_QZa=drho_qZa,
                   chi_Qwab=chi_qwab,
                   phi_qaZ=phi_qaZ,
                   qcomm=qcomm,
                   qmax=qmax,
                   wmax=wmax,
                   timer=timer)

    def interpolate_to_Z_grid(self, Z_z):
        drho_qza = np.zeros(shape=(len(self.q_q), len(Z_z), self.aN),
                            dtype=complex)
        ind_z = np.logical_and(Z_z >= self.Z_Z[0], Z_z <= self.Z_Z[-1])
        drho_qza[:, ind_z, :] = self.drho_spline_qxa(Z_z[ind_z])
        return drho_qza

    @timer('Solve Poisson equation')
    def get_induced_potential(self):
        """
        Solves poissons equation in 1D using Green's function:
                   /
        dphi(z) =  |  G(q, z, z') drho(z') dz'
                   /
        The potential is calculated on the same grid as drho_z.
        The Green's function goes to zero for z -> ±∞, and thus implements
        Dirichlet boundary conditions.
        Note that the problem is ill-defined for q=0, so a small-but-finite
        q is required in the q -> 0 limit.
        dphi: induced potential
        drho: induced density basis function
        q: momentum transfer.
        z: z-grid on which the equation is solved
        """
        q_q = self.q_q.copy()
        q_q[q_q == 0] = 1e-12
        Delta_qZZ = np.exp(-self.q_q[:, None, None] *
                           np.abs(self.Z_Z[None, :, None] -
                                  self.Z_Z[None, None, :]))
        prefactor_q = 2 * np.pi / q_q * self.dZ
        dphi_qZa = Delta_qZZ @ self.drho_qZa * prefactor_q[:, None, None]

        ddphil_qa = +self.q_q[:, None] * dphi_qZa[:, 0, :]
        ddphir_qa = -self.q_q[:, None] * dphi_qZa[:, -1, :]

        bc = [(1, ddphil_qa), (1, ddphir_qa)]

        interp = CubicSpline(x=self.Z_Z,
                             y=dphi_qZa,
                             axis=1,
                             bc_type=bc)

        return dphi_qZa, interp

    def interpolate_potential(self,
                              Z_z: np.ndarray,
                              left: np.ndarray,
                              middle: np.ndarray,
                              right: np.ndarray,
                              a: float,
                              b: float) -> np.ndarray:
        """ interpolates induced potential from building block grid (Z_Z) to
        the large heterostructure grid (centered around z0) Z_z, utilizing the
        fact that away from the monolayer, the induced potential must decay
        exponentially.
        XXX: The BB grid must be centered on the position z0 of the BB.

        Parameters
        ----------
        Z_z: np.ndarray
            the large heterostructure grid (z_z - z0)
        left: np.ndarray
            the indices to the left of the monolayer z-grid
        middle: np.ndarray
            the indices of inside the monolayer z-grid
        right: np.ndarray
            the indices of to the right of the monolayer z-grid
        a: float
            the leftmost monolayer z-grid value
        b: float
            the rightmost monolayer z-grid value
        """

        dphi_qza = np.zeros((len(self.q_q), len(Z_z), self.aN), dtype=complex)

        dphi_qza[:, left, :] = self.dphi_qZa[:, 0][:, None, :] \
            * np.exp(-self.q_q[:, None, None] * (a - Z_z[None, left, None]))
        dphi_qza[:, right, :] = self.dphi_qZa[:, -1][:, None, :] \
            * np.exp(-self.q_q[:, None, None] * (Z_z[None, right, None] - b))

        dphi_qza[:, middle, :] = self.dphi_spline_qxa(Z_z[middle])

        return dphi_qza

    def write(self, filename, format=None):
        data = {
            'q_q': self.q_q,
            'omega_w': self.omega_w,
            'z_z': self.Z_Z,
            'z0': 0.0,
            'drho_qza': self.drho_qZa,
            'chi_qwab': self.chi_qwab,
            'phi_qaz': self.phi_qaZ
        }
        return write_file(filename=filename, data=data, format=format)


class Layer:
    def __init__(self,
                 z0: float,
                 layerwidth: float,
                 z_z: np.ndarray,
                 bb: BuildingBlock):
        r"""
        Initialize a layer in the heterostructure. The layer is defined
        by the position z0, the thickness layerwidth and a building block.
        The layer is centered at z0.

        Parameters
        ----------
        z0: float
            position of the layer
        layerwidth: float
            thickness of the layer
        z_z: np.ndarray
            z grid of the entire heterostructure
        bb: BuildingBlock
            corresponding building block

        Attributes
        ----------
        z_Z: np.ndarray
            z grid of the layer
        z_z: np.ndarray
            z grid of the entire heterostructure
        z0: float
            position of the layer in the heterostructure
        layerwidth: float
            thickness of the layer
        bb: BuildingBlock
            corresponding building block
        a: float
            value at the left edge of the z_Z grid
        b: float
            value at the right edge of the z_Z grid
        left: np.ndarray
            z_z grid points to the left of the layer z grid
        right: np.ndarray
            z_z grid points to the right of the layer z grid
        middle: np.ndarray
            z_z grid points in the inside of the layer z grid

        Class responsibilities
        ----------------------
        - Calculations on single layers in terms of the z_z and z_Z grid.
        - Communicating z_z with the building blocks
        """

        self.z0 = z0
        self.layerwidth = layerwidth
        self.bb = bb
        self.z_Z = bb.Z_Z + z0
        self.z_z = z_z
        self.a = self.z_Z[0]
        self.b = self.z_Z[-1]
        self.left = self.z_z < self.a
        self.right = self.z_z > self.b
        self.middle = ~(self.left + self.right)
        assert (self.z_z[self.left] < self.a).all()
        assert (self.z_z[self.right] > self.b).all()

    @cached_property
    def drho_qza(self) -> np.ndarray:
        return self.bb.interpolate_to_Z_grid(self.z_z - self.z0)

    @cached_property
    def dphi_qza(self) -> np.ndarray:
        dphi_qza = self.bb.interpolate_potential(self.z_z - self.z0,
                                                 self.left,
                                                 self.middle,
                                                 self.right,
                                                 self.a - self.z0,
                                                 self.b - self.z0)
        return dphi_qza

    @cached_property
    def phi_qaz(self) -> np.ndarray:
        middle = np.logical_and(self.z_z > (self.z0 - self.layerwidth / 2),
                                self.z_z < (self.z0 + self.layerwidth / 2))
        phi_qaz = np.zeros((len(self.bb.q_q), self.bb.aN, len(self.z_z)), dtype=complex)
        phi_qaz[:, :, middle] = self.bb.phi_spline_qaZ(self.z_z[middle] - self.z0)

        return phi_qaz
