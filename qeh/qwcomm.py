from ase.parallel import world
import numpy as np


class QComm:
    # Serial communicator
    def __init__(self, QN):
        self.QN = QN
        self.myqN = QN
        self.my_q = np.arange(self.myqN)
        self.q1 = 0
        self.q2 = self.myqN
        self.my_q = np.arange(self.q1, self.q2)
        self.size = 1
        self.rank = 0

    def all_gather_qX(self, a_qX):
        """ Collect arrays of dim q along its first axis. """
        return a_qX

    def broadcast(self, a_X, root=0):
        return


class QCommMPI(QComm):
    # MPI communicator
    def __init__(self, QN):
        self.QN = QN
        self.myqN = (self.QN + world.size - 1) // world.size
        self.q1 = min(self.myqN * world.rank, self.QN)
        self.q2 = min(self.q1 + self.myqN, self.QN)
        self.my_q = np.arange(self.q1, self.q2)
        self.size = world.size
        self.rank = world.rank

    def all_gather_qX(self, a_qX):
        """ Collect arrays of dim q along its first axis. """
        qX = np.array(a_qX.shape)
        qX[0] = self.myqN
        QX = qX.copy()
        QX[0] = self.myqN * world.size

        b_qX = np.zeros_like(a_qX, shape=qX)
        b_qX[:self.q2 - self.q1] = a_qX
        A_QX = np.empty_like(a_qX, shape=QX)
        if world.size == 1:
            A_QX[:] = b_qX
        else:
            world.all_gather(b_qX, A_QX)
        return A_QX[:self.QN]

    def broadcast(self, a_X, root=0):
        world.broadcast(a_X, root)
        return
