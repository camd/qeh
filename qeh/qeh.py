from __future__ import print_function
from typing import Union

from pathlib import Path
import pickle
import numpy as np
import warnings
from ase.utils.timing import timer

from qeh.substrate import Substrate
from qeh.bb_calculator.bb_interpolator import interpolate_building_blocks
from qeh.heterostructure import Heterostructure
from qeh.qwcomm import QComm


def load(fd):
    try:
        return pickle.load(fd, encoding='latin1')
    except TypeError:
        return pickle.load(fd)


class QEH:
    """This class defines dielectric function of heterostructures
        and related physical quantities."""

    def __init__(self,
                 hs: Heterostructure):
        r""" Initialize the QEH calculator for a heterostructure.

        Parameters
        ----------
        hs: Heterostructure
            the heterostructure on which to calculate.

        Array subscripts:
        -----------
        _q : qpoints
        _w : frequency
        _z : heterostructure z-grid
        _Z : local z-grid, e.g. grids used in the building blocks
        _i, _ij : heterostructure basis functions
        _a, _ab : basis functions for a single BB (without layer index)
        _l : layers
        _n : unique building blocks

        """

        self.timer = hs.timer
        self.hs = hs

        self.kernel_qwij = self.hs.kernel_qwij

    @classmethod
    def heterostructure(cls,
                        BBfiles: list[str],
                        layerwidth_n: Union[list[float],
                                            np.ndarray, None] = None,
                        layerwidth_l: Union[list[float],
                                            np.ndarray, None] = None,
                        qmax: Union[float, None] = None,
                        wmax: Union[float, None] = None,
                        amax: Union[int, None] = None,
                        dz: Union[float, None] = None,
                        substrate: Union[Substrate, None] = None,
                        qcomm: Union[QComm, None] = None,
                        include_overlap: bool = True):
        r""" Initialize a qeh from a list of building block
        files. The building blocks should be on the same grid.

        Parameters
        ----------
        BBfiles: list of str
            list of names of the building block files
        layerwidth_n: list of float | None
            list of the thicknesses of the building blocks
        layerwidth_l: list of float | None
            list of the thicknesses of the layers
        qmax: float | None
            maximum q value of the heterostructure
        wmax: float | None
            maximum frequency value of the heterostructure
        amax: int | None
            maximum number of basis functions in the building blocks
        substrate: Substrate | None
            substrate descriptor object
        qcomm: QComm | None
            q communicator object for parallelization. If
            None, a default QCommMPI object is created,
            which distributes the q-points over all processes.
        Bohr: float | None
            Value of 1 atomic Bohr in the building block length units.
            Only relevant if one uses default thicknesses or emasses.
        """

        hs = Heterostructure(BBfiles=BBfiles,
                             layerwidth_n=layerwidth_n,
                             layerwidth_l=layerwidth_l,
                             qmax=qmax,
                             wmax=wmax,
                             amax=amax,
                             dz=dz,
                             substrate=substrate,
                             qcomm=qcomm,
                             include_overlap=include_overlap)

        return cls(hs=hs)

    @timer('Solve Poisson equation')
    def solve_poisson_1D(self, drho_z, q, z):
        """
        Solves poissons equation in 1D using Green's function:
                   /
        dphi(z) =  |  G(q, z, z') drho(z') dz'
                   /
        The potential is calculated on the same grid as drho_z.
        The Green's function goes to zero for z -> ±∞, and thus implements
        Dirichlet boundary conditions.
        Note that the problem is ill-defined for q=0, so a small-but-finite
        q is required in the q -> 0 limit.
        dphi: induced potential
        drho: induced density basis function
        q: momentum transfer.
        z: z-grid on which the equation is solved

        XXX: This is currently only used by
        _get_exciton_screened_potential_grid(),
        maybe we should just move it there.
        """

        Delta_zz = np.abs(z.reshape(-1, 1) - z)
        dz = z[1] - z[0]
        prefactor = 2 * np.pi / q * dz
        dphi_z = np.exp(-q * Delta_zz) @ drho_z * prefactor
        return dphi_z

    @timer('Solve chi dyson equation')
    def get_chi_matrix(self, iq_q=None, iw_w=None):
        """
        Gets the chi matrix chi_qwij for the entire heterostructure by solving
        the Dyson equation: chi_full = chi_intra + chi_intra V_inter chi_full.
        """
        if iq_q is None:
            iq_q = np.arange(self.hs.qN)

        if iw_w is None:
            iw_w = np.arange(self.hs.wN)

        eye_ij = np.eye(self.hs.ndim)
        Vinter_qwij = self.hs.get_interlayer_Coulomb_kernel()
        if Vinter_qwij.shape[1] > 1:
            Vinter_qwij = Vinter_qwij[:, iw_w]
        Vinter_qwij = Vinter_qwij[iq_q]
        chi_intra_qwij = self.hs.chi_intra_qwij[:, iw_w]
        chi_intra_qwij = chi_intra_qwij[iq_q]
        chi_qwij = np.linalg.solve(
            eye_ij - chi_intra_qwij @ self.hs.gphi_qij[iq_q, None, :, :]
            @ Vinter_qwij @ self.hs.grho_qij[iq_q, None, :, :], chi_intra_qwij)

        return chi_qwij

    @timer('Get inverse dielectric matrix')
    def get_inverse_dielectric_matrix(self, iq_q=None, iw_w=None):
        """
        Get dielectric matrix as: eps^{-1} = 1 + V chi_full
        """

        if iq_q is None:
            iq_q = np.arange(self.hs.qN)

        if iw_w is None:
            iw_w = np.arange(self.hs.wN)

        chi_qwij = self.get_chi_matrix(iq_q, iw_w)

        if self.kernel_qwij.shape[1] == 1:
            # This occurs when there is no substrate; then the Coulomb kernel
            # will not be frequency dependent, its shape will be (q, 1, i, j)
            kernel_qwij = self.kernel_qwij[iq_q]
        else:
            kernel_qwij = self.kernel_qwij[:, iw_w]
            kernel_qwij = kernel_qwij[iq_q]

        epsinv_qwij = np.linalg.inv(self.hs.gphi_qij[iq_q, None]) \
            + kernel_qwij @ self.hs.grho_qij[iq_q, None] @ chi_qwij

        return epsinv_qwij

    def get_eps_matrix(self, iq_q=None, iw_w=None):
        epsinv_qwij = self.get_inverse_dielectric_matrix(iq_q, iw_w)
        return np.linalg.inv(epsinv_qwij)

    @timer('Get screened potential')
    def get_screened_potential(self, layer=0,
                               subtract_bare_coulomb=False):
        r"""
        get the screened interaction averaged over layer "l":
        W_{kk}(q, w) = \sum_{ij} V_{ki}(q) \chi_{ij}(q, w) V_{jk}(q)

        parameters:
        layer: int
            index of layer to calculate the screened interaction for.

        returns: W(q,w)
        """

        chi_qwij = self.get_chi_matrix()

        i = 2 * layer  # XXX: Bad, and assumes 2 basis functions.

        kernel_qwij = self.kernel_qwij
        W_qw = kernel_qwij[:, :, [i], :] @ self.hs.grho_qij[:, None, :, :]\
            @ chi_qwij @ self.hs.gphi_qij[:, None, ...] @ kernel_qwij[:, :, :, [i]]
        W_qw = W_qw[:, :, 0, 0]

        V0_qw = kernel_qwij[:, :, i, i]

        if subtract_bare_coulomb is False:
            W_qw += V0_qw

        W_qw = self.hs.qcomm.all_gather_qX(W_qw)

        return W_qw

    def parse_ehdistr(self, distr: Union[int, list, np.ndarray]):
        """
        Parses the input distribution (distr) to determine the electron or
        hole density.

        Returns:
            A dictionary describing the parsed distribution with keys:
            - 'type': 'monopole' or 'grid'
            - 'distr': the processed distribution array
        """

        def layer_to_basis_index(distr_l):
            """
            Converts a layer-based distribution (distr_l) to a
            basis-based distribution (distr_i).

            Args:
                distr_l (np.ndarray): Array specifying the distribution across
                layers.

            Returns:
                np.ndarray: Array specifying the distribution in the monopole
                basis.
            """
            distr_i = np.zeros(
                self.hs.ndim)  # Initialize distribution in monopole basis
            idx = 0
            for l, layer in enumerate(self.hs.layers_l):
                distr_i[idx] = distr_l[l]
                # Increment index by the number of basis functions in the layer
                idx += layer.bb.aN
            return distr_i

        dct = {}

        # Step 1: Handle integer input (single layer index)
        if isinstance(distr, int):
            # Initialize layer-based distribution
            distr_l = np.zeros(self.hs.nlayers)
            distr_l[distr] = 1  # Set the specified layer index to 1
            distr_i = layer_to_basis_index(
                distr_l)  # Convert to monopole basis
            dct['type'] = 'monopole'
            dct['distr'] = distr_i

        # Step 2: Handle array-like input (layer-based or
        #         grid-based distribution)
        else:
            # Distribution matches number of layers
            if len(distr) == len(self.hs.layers_l):
                dct['type'] = 'monopole'
                distr_i = layer_to_basis_index(
                    distr)  # Convert to monopole basis
                dct['distr'] = distr_i

            # Distribution matches number of monopole basis functions
            elif len(distr) == self.hs.ndim:
                dct['type'] = 'monopole'
                dct['distr'] = distr

            else:  # Assume grid-based distribution
                dct['type'] = 'grid'
                dct['distr'] = np.atleast_2d(distr)  # Ensure 2D array

        # Step 3: Validate grid-based distribution
        if dct['type'] == 'grid':
            shape = dct['distr'].shape
            assert shape[0] in [1, len(self.hs.q_q)], \
                "Invalid grid shape: incompatible q-dimension"
            assert shape[-1] == len(self.hs.z_z), \
                "Invalid grid shape: incompatible z-dimension"

        return dct

    @timer('Get exciton screened potential')
    def get_exciton_screened_potential(self,
                                       e_distr: Union[int, list, np.ndarray],
                                       h_distr: Union[int, list, np.ndarray],
                                       symmetrize=False):
        """
        Calculates the interaction energy between and electron and hole
        E = <rho_e | W | rho_i>, where W = V + V chi V.

        If symmetrize: solves the Poisson equation for both electron and hole,
        and calculates the interaction as 1/2 (<rho_e | W_h> + <W_e | rho_h>)

        The electron and hole densities can be specified in various forms:
        1. An integer indicating the layer index where the electron/hole
           resides.
           Example: e_distr = 0, h_distr = 1

        2. A list or array with length equal to the number of layers,
           specifying the distribution of the electron/hole across layers.
           Example: e_distr = [1, 0], h_distr = [0, 1]

        3. A grid-based distribution as a numpy array (f_z or f_qz):
           - f_z: 1D array with length equal to self.hs.z_z
           - f_qz: 2D array with shape (nq, nz), where nq = len(self.hs.q_q)
             and nz = len(self.hs.z_z)

        Note that the electron and hole densities must be specified in similar
        ways; either both on a grid, or both in terms of monopole densities.
        """

        epsinv_qij = self.get_inverse_dielectric_matrix(iw_w=[0])[:, 0]
        kernel_qij = self.kernel_qwij[:, 0]
        e_distr = self.parse_ehdistr(e_distr)
        h_distr = self.parse_ehdistr(h_distr)
        assert e_distr['type'] == h_distr['type'], \
            'Electron and hole densities must be specified in the same way.'
        if e_distr['type'] == 'grid':
            return self._get_exciton_screened_potential_grid(
                e_distr_qz=e_distr['distr'],
                h_distr_qz=h_distr['distr'],
                symmetrize=symmetrize)
        e_distr_i = e_distr['distr']
        h_distr_i = h_distr['distr']
        # Normalize electron and hole densities
        # XXX: There has to be a better way to do this
        # XXX: Having to convert the densities to the z-grid
        # XXX: is not very clean. It is fine in the case where you
        # XXX: only have a few density distributions, however,
        # XXX: in a full BSE calculation, this is not the case.
        e_norm_q = (self.hs.drho_qzi @ e_distr_i).sum(1) * self.hs.dz
        e_distr_qi = e_distr_i / e_norm_q[:, None]
        h_norm_q = (self.hs.drho_qzi @ h_distr_i).sum(1) * self.hs.dz
        h_distr_qi = h_distr_i / h_norm_q[:, None]
        # Calculate the induced potential overlap with the electron density
        dphi_qzi = self.hs.dphi_qzi
        drho_qiz = self.hs.drho_qzi.conj().transpose(0, 2, 1)
        if self.hs.substrate is not None:
            dphi_qzi = dphi_qzi + self.hs.substrate.get_static_image_potential(
                dphi_qzi, self.hs.z_z, self.hs.q_q, self.hs.layers_l[0].z0)
        drp_qij = drho_qiz @ dphi_qzi * self.hs.dz

        # Calculate the interaction energy in our pole basis
        rWr_qij = drp_qij @ \
            np.linalg.solve(kernel_qij,
                            epsinv_qij @ self.hs.gphi_qij @ kernel_qij)
        if symmetrize:
            rWr_qij = rWr_qij / 2 + rWr_qij.conj().transpose(0, 2, 1) / 2
        # Sum over the electron and hole basis functions to get
        # the interaction energy
        v_screened_q = np.einsum('qi, qij, qj -> q',
                                 e_distr_qi, rWr_qij, h_distr_qi,
                                 optimize='optimal')
        v_screened_q = v_screened_q.real

        v_screened_q = self.hs.qcomm.all_gather_qX(v_screened_q)

        return -v_screened_q

    def _get_exciton_screened_potential_grid(self,
                                             e_distr_qz: np.ndarray,
                                             h_distr_qz: np.ndarray,
                                             symmetrize=False):
        """
        calculates W_q = <rho_e | W | rho_h> by solving the Poisson equation
        on a real-space grid. This function is similar to
        get_exciton_screened_potential, except that the electron and hole
        distributions are given as arbitrary functions of q and z, rather than
        in terms of the density basis functions.
        If symmetrize: solves the Poisson equation for both electron and hole,
        and calculates the interaction as 1/2 (<rho_e | W_h> + <W_e | rho_h>)
        """
        assert self.hs.qcomm.size == 1, 'parallelism not implemented!'
        z_z = self.hs.z_z

        # parse input
        Nz = len(z_z)
        Nq = self.hs.qN
        eshape = e_distr_qz.shape
        assert eshape[-1] == Nz
        if eshape[0] == 1:
            new_eshape = (Nq, Nz)
            e_distr_qz = e_distr_qz.reshape(1, -1)
            e_distr_qz = np.tile(e_distr_qz, (Nq, 1))
            assert e_distr_qz.shape == new_eshape

        hshape = h_distr_qz.shape
        assert hshape[-1] == Nz
        if hshape[0] == 1:
            new_hshape = (Nq, Nz)
            h_distr_qz = h_distr_qz.reshape(1, -1)
            h_distr_qz = np.tile(h_distr_qz, (Nq, 1))
            assert h_distr_qz.shape == new_hshape

        # calculate potential from hole distribution by solving Poisson
        # equation, and project the solution onto the potential basis
        # functions to find the induced densities
        dphi_qz = np.zeros((Nq, Nz), dtype=complex)
        dphi_qi = np.zeros((Nq, self.hs.ndim), dtype=complex)
        chi_qij = self.get_chi_matrix(iw_w=[0])[:, 0]
        for iq, q in enumerate(self.hs.q_q):
            dphi_qz[iq] = self.solve_poisson_1D(h_distr_qz[iq], q, z_z)
            dphi_qi[iq] = self.hs.project_on_phi(dphi_qz[iq], iq=iq,
                                                 return_coefficients=True)

        # add image charges if substrate is present
        if self.hs.substrate is not None:
            dphi_image_qz = self.hs.substrate.get_static_image_potential(
                dphi_qz[:, :, None], self.hs.z_z, self.hs.q_q,
                self.hs.layers_l[0].z0)
            dphi_qz += dphi_image_qz
            for iq, q in enumerate(self.hs.q_q):
                dphi_qi += self.hs.project_on_phi(
                    dphi_qz[iq], iq=iq, return_coefficients=True)
        drho_ind_qi = np.einsum('qij, qjk, qk -> qi', self.hs.grho_qij,
                                chi_qij, dphi_qi)
        dphi_ind_qz = np.einsum('qzi, qi -> qz', self.hs.dphi_qzi, drho_ind_qi)
        dphi_tot_qz = dphi_ind_qz + dphi_qz

        W_q = -np.sum(e_distr_qz.conj() * dphi_tot_qz, axis=1) * self.hs.dz
        if symmetrize:
            #  we conjugate the densities;
            #  this corresponds to interchanging the bra and ket
            W2_q = self._get_exciton_screened_potential_grid(
                e_distr_qz=h_distr_qz.conj(),
                h_distr_qz=e_distr_qz.conj(),
                symmetrize=False)
            W_q = W_q / 2 + W2_q / 2

        return W_q

    def get_exciton_screened_potential_r(self, r_array, e_distr, h_distr,
                                         Wq_data=None, intralayer=None,
                                         symmetrize=False):
        from scipy.interpolate import CubicSpline
        from mpmath import hyp2f3
        from qeh.tools.integrate_j0_spline import integrate_j0_spline
        assert self.hs.qcomm.size == 1, 'parallelism not implemented!'
        if Wq_data is not None:
            if isinstance(Wq_data, str):
                q_q, W_q = load(open(Wq_data, 'rb'))
            else:
                q_q, W_q = Wq_data
        else:
            q_q = self.hs.q_q
            W_q = self.get_exciton_screened_potential(
                e_distr, h_distr, symmetrize=symmetrize)

        # Building block part
        q0 = q_q.max()
        WR_q = CubicSpline(q_q, W_q * q_q)
        W_r = integrate_j0_spline(WR_q, r_array)

        # Analytical correction
        if intralayer is None:
            assert len(e_distr) == self.hs.ndim, \
                '\"Intralayer\" cannot be detected automatically. '\
                'Please specify whether your exciton is intralayer, or not.'
            intralayer = True
            if np.max(e_distr) != 1.0:
                intralayer = False
            if np.max(h_distr) != 1.0:
                intralayer = False
            if np.sum(h_distr * e_distr) != 1.0:
                intralayer = False
            if not intralayer:
                warnings.warn(
                    'Analytic correction only implemented for '
                    'intralayer excitons!')

        if intralayer:
            hyperhyper = np.zeros_like(r_array)
            for i, r in enumerate(r_array):
                hyperhyper[i] = hyp2f3(1, 1, 2, 2, 2, - (q0 * r) ** 2 / 4)
            W2_r = -np.euler_gamma + np.log(2 / (r_array * q0)) \
                + (q0 * r_array)**2 * hyperhyper / 8
            W_r = W_r + W2_r * (W_q[-1] * q_q[-1]**2)

        return r_array, W_r / (2 * np.pi)

    def get_exciton_binding_energies(self, eff_mass, e_distr, h_distr,
                                     L_min=-50, L_max=10, Delta=0.1,
                                     Wq_data=None, symmetrize=False,
                                     intralayer=None):
        """Find exciton binding energy from Mott-Wannier model
        using the screened interaction
        """
        r_space = np.arange(L_min, L_max, Delta)  # log of radial grid

        # real-space screened interaction on radial grid
        r, W_r = self.get_exciton_screened_potential_r(r_array=np.exp(r_space),
                                                       e_distr=e_distr,
                                                       h_distr=h_distr,
                                                       Wq_data=None,
                                                       symmetrize=symmetrize,
                                                       intralayer=intralayer)
        # construct hydrogen-like Hamiltonian and solve
        r_r = np.exp(r_space)
        rr_r = r_r[1:] * r_r[:-1]
        H_rr = np.diag(W_r - (-2 / Delta**2 + 0.25) / (2 * r_r**2 * eff_mass))\
            - np.diag((1. / Delta**2 - 1. / (2 * Delta))
                      / (2 * rr_r * eff_mass), k=1)\
            - np.diag((1. / Delta**2 + 1. / (2. * Delta))
                      / (2 * rr_r * eff_mass), k=-1)

        ee, ev = np.linalg.eig(H_rr)
        index_sort = np.argsort(ee.real)
        ee = ee[index_sort]  # find lowest eigenvalue and corresp. eigenvector
        ev = ev[:, index_sort]
        return ee, ev

    def get_macroscopic_dielectric_function(self, static=True, direction='x'):
        """
        Calculates the averaged dielectric function over the structure.

        Parameters:

        static: bool
            If True only include w=0

        direction: str 'x' or 'z'
            'x' for in plane dielectric function
            'z' for out of plane dielectric function

        Returns list of q-points, frequencies, dielectric function(q, w).

        Vext_i is the external potential applied to the heterostructure.
        It is assumed to be constant (x) / linear(z) in the z-direction.
        The average is calculated by projecting the total potential
        Vtot = eps^{-1}Vext onto Vext and summing over all layers, each layer
        weighted by its width.
        """
        assert direction in {'x', 'z'}
        assert self.hs.substrate is None, \
            'get_macroscopic_dielectric_function does not '\
            'apply for structures with substrate'

        if static:
            assert np.isclose(self.hs.omega_w[0], 0)
            iw_w = [0]
        else:
            iw_w = None
        omega_w = self.hs.omega_w[iw_w]

        if direction == 'z':
            q_Q = self.hs.qcomm.all_gather_qX(self.hs.q_q)
            assert q_Q[0] * self.hs.layerwidth_l.sum() < 0.01, \
                'epsilon_z only well-defined in q->0 limit!'
            if self.hs.qcomm.rank == 0:
                epsM_w = self._get_macroscopic_dielectric_function_z(iw_w)
            else:
                epsM_w = np.zeros(len(omega_w), dtype=complex)
            self.hs.qcomm.broadcast(epsM_w)
            return q_Q[0], omega_w, epsM_w

        elif direction == 'x':
            epsM_qw = self._get_macroscopic_dielectric_function_x(iw_w)
            epsM_Qw = self.hs.qcomm.all_gather_qX(epsM_qw)
            q_Q = self.hs.qcomm.all_gather_qX(self.hs.q_q)
            return q_Q, self.hs.omega_w[iw_w], epsM_Qw

    def _get_macroscopic_dielectric_function_z(self, iw_w=None, inverse=True):
        Vext_i = self.hs.get_dipole_potential(normalize=False, iq=0)
        chi_wij = self.get_chi_matrix(iq_q=[0], iw_w=iw_w)[0]
        dphi_zi = self.hs.dphi_qzi[0]
        grho_ij = self.hs.grho_qij[0]
        phi_ind_wz = dphi_zi @ grho_ij @ chi_wij @ Vext_i
        L = np.sum(self.hs.layerwidth_l)
        Delta_phi_ind_w = phi_ind_wz[:, -1] - phi_ind_wz[:, 0]

        epsinv_w = 1 + Delta_phi_ind_w / L
        eps_w = (1 / epsinv_w).real if inverse else epsinv_w
        return eps_w

    def _get_macroscopic_dielectric_function_x(self, iw_w=None, inverse=True):
        epsinv_qwij = self.get_inverse_dielectric_matrix(iw_w=iw_w)
        epsinvM_qw = np.zeros_like(epsinv_qwij[..., 0, 0])
        for iq, epsinv_wij in enumerate(epsinv_qwij):
            Vext_i = self.hs.get_monopole_potential(normalize=True, iq=iq)
            epsinvM_qw[iq] = Vext_i @ epsinv_wij @ Vext_i
        epsM_qw = (1. / epsinvM_qw).real if inverse else epsinvM_qw
        return epsM_qw

    def get_dielectric_function(self, layer=0):
        """
        Calculates the dielectric function of the chosen layer

        Parameters:
        layer: int
            index of layer to calculate the dielectric function for.

        Returns list of q-points, frequencies, dielectric function(q, w).
        """
        raise NotImplementedError('Not refactored!')

        W_qw = self.get_screened_potential(layer=layer)
        kernel_qwij = self.kernel_qwij

        k = layer
        if self.chi_dipole_nqw is not None:
            k *= 2

        # XXX jks: before this did not use substrate, and only used a static
        # kernel. does that make sense to do?
        kernel_qw = kernel_qwij[:, :, k, k]
        eps_qw = kernel_qw / W_qw
        return self.q_q, self.omega_w, eps_qw

    def get_eels(self, dipole_contribution=False):
        r"""
        Calculates Electron energy loss spectrum, defined as:

        EELS(q, w) = - Im 4 \pi / q**2 \chiM(q, w)

        Returns list of q-points, Frequencies and the loss function
        """
        if dipole_contribution:
            raise NotImplementedError()
            Vext_i = self.hs.get_dipole_potential(normalize=True)
        else:
            eels_qw = self._get_macroscopic_dielectric_function_x(
                inverse=False)

        eels_qw /= self.hs.q_q[:, None]

        eels_Qw = self.hs.qcomm.all_gather_qX(eels_qw)
        q_Q = self.hs.qcomm.all_gather_qX(self.hs.q_q)

        return q_Q, self.hs.omega_w, -eels_Qw.imag

    def get_absorption_spectrum(self, dipole_contribution=False):
        r"""
        Calculates absorption spectrum, defined as:

        ABS(q, w) = - Im 2 / q \epsM(q, w)

        Returns list of q-points, Frequencies and the loss function
        """
        if dipole_contribution:
            raise NotImplementedError()
            Vext_i = self.hs.get_dipole_potential(normalize=True)
        else:
            abs_qw = self._get_macroscopic_dielectric_function_x(inverse=False)

        abs_qw = 1 / abs_qw
        abs_qw *= 2. / self.hs.q_q[:, None]

        abs_Qw = self.hs.qcomm.all_gather_qX(abs_qw)
        q_Q = self.hs.qcomm.all_gather_qX(self.hs.q_q)
        return q_Q, self.hs.omega_w, abs_Qw.imag

    def get_sum_eels(self, V_beam=100, include_z=False):
        r"""
        Calculates the q- averaged Electron energy loss spectrum usually
        obtained in scanning transmission electron microscopy (TEM).

        EELS(w) = - Im [sum_{q}^{q_max}  V(q) \chi(w, q) V(q)]
                    \delta(w - q \dot v_e)

        The calculation assumes a beam in the z-direction perpendicular to the
        layers, and that the response in isotropic within the plane.

        Input parameters:
        V_beam: float
            Acceleration voltage of electron beam in kV.
            Is used to calculate v_e that goes into \delta(w - q \dot v_e)

        Returns list of Frequencies and the loss function
        """
        raise NotImplementedError('Not refactored!')
        import ase.units
        from ase.units import Hartree, Bohr
        const_per = np.ones([self.n_layers])
        layer_weight = self.s / np.sum(self.s) * self.n_layers

        if self.chi_dipole_nqw is not None:
            const_per = np.insert(const_per,
                                  np.arange(self.n_layers) + 1,
                                  np.zeros([self.n_layers]))
            layer_weight = np.insert(layer_weight,
                                     np.arange(self.n_layers) + 1,
                                     layer_weight)

        eels_w = np.zeros([len(self.omega_w)], dtype=complex)
        chi_qwij = self.get_chi_matrix()
        vol = np.pi * (self.q_q[-1] + self.q_q[1] / 2.)**2
        weight0 = np.pi * (self.q_q[1] / 2.)**2 / vol
        c = (1 - weight0) / np.sum(self.q_q)
        weights = c * self.q_q
        weights[0] = weight0
        # Beam speed from relativistic eq
        me = ase.units._me
        c = ase.units._c
        E_0 = me * c**2  # Rest energy
        E = E_0 + V_beam * 1e3 / ase.units.J   # Relativistic energy
        v_e = c * (E**2 - E_0**2)**0.5 / E  # beam velocity in SI
        # Lower cutoff q_z = w / v_e
        w_wSI = self.omega_w * Hartree \
            / ase.units.J / ase.units._hbar  # w in SI units
        q_z = w_wSI / v_e / ase.units.m * Bohr  # in Bohr
        q_z[0] = q_z[1]
        print('Using a beam acceleration voltage of V = %3.1f kV' % (V_beam))
        print('Beam speed = %1.2f / c' % (v_e / c))
        # Upper cutoff q_c = q[1] / 2.
        q_c = self.q_q[1] / 2.
        # Integral for q=0: \int_0^q_c \frac{q^3}{(q^2 + q_z^2)^2} dq
        I1 = 2 * np.pi / vol * \
            (q_z**2 / 2. / (q_c**2 + q_z**2) - 0.5 +
             0.5 * np.log((q_c / q_z)**2 + 1))
        I2 = 2 * np.pi / vol / 2. * (1. / q_z**2 - 1. / (q_z**2 + q_c**2))

        for iq in range(self.mynq):
            eels_temp = np.zeros([len(self.omega_w)], dtype=complex)
            for iw in range(len(self.omega_w)):
                # Longitudinal in-plane
                temp = np.dot(np.array(const_per) * layer_weight,
                              np.dot(chi_qwij[iq, iw], np.array(const_per)))
                eels_temp[iw] += temp

            if np.isclose(self.q_q[iq], 0):
                eels_temp *= (4 * np.pi)**2 * I1

            else:
                eels_temp *= 1. / (self.q_q[iq]**2 + q_z**2)**2
                eels_temp *= (4 * np.pi)**2 * weights[iq]
            eels_w += eels_temp

            if include_z:
                eels_temp = np.zeros([len(self.omega_w)], dtype=complex)
                for iw in range(len(self.omega_w)):
                    # longitudinal out of plane
                    temp = np.dot(np.array(const_per[::-1]) * layer_weight,
                                  np.dot(chi_qwij[iq, iw],
                                         np.array(const_per[::-1])))
                    eels_temp[iw] += temp

                    # longitudinal cross terms
                    temp = 1J * np.dot(np.array(const_per) * layer_weight,
                                       np.dot(chi_qwij[iq, iw],
                                              np.array(const_per[::-1])))
                    eels_temp[iw] += temp / q_z[iw]

                    temp = -1J * np.dot(np.array(const_per[::-1]) *
                                        layer_weight,
                                        np.dot(chi_qwij[iq, iw],
                                               np.array(const_per)))
                    eels_temp[iw] += temp / q_z[iw]

                    # Transversal
                    temp = np.dot(np.array(const_per[::-1]) * layer_weight,
                                  np.dot(chi_qwij[iq, iw],
                                         np.array(const_per[::-1])))
                    temp *= (v_e / c)**4
                    eels_temp[iw] += temp

                if np.isclose(self.q_q[iq], 0):
                    eels_temp *= (4 * np.pi)**2 * I2 * q_z**2
                else:
                    eels_temp *= 1. / (self.q_q[iq]**2 + q_z**2)**2\
                        * q_z**2
                    eels_temp *= (4 * np.pi)**2 * weights[iq]

                eels_w += eels_temp

        # self.world.sum(eels_w)

        return self.omega_w * Hartree, - (Bohr**5 * eels_w * vol).imag

    def get_response(self, iw=0, dipole=False):
        r"""
        Get the induced density and potential due to constant perturbation
        obtained as: rho_ind(r) = int chi(r,r') dr'
        """
        raise NotImplementedError('Not refactored!')
        const_per = np.ones([self.n_layers])
        if self.chi_dipole_nqw is not None:
            const_per = np.insert(const_per,
                                  np.arange(self.n_layers) + 1,
                                  np.zeros([self.n_layers]))

        if dipole:
            const_per = self.z0_l - self.z0_l[-1] / 2.
            const_per = np.insert(const_per,
                                  np.arange(self.n_layers) + 1,
                                  np.ones([self.n_layers]))

        chi_qij = self.get_chi_matrix()[:, iw]
        Vind_qz = np.zeros((self.mynq, len(self.z_z)))
        rhoind_qz = np.zeros((self.mynq, len(self.z_z)))

        drho_array = self.drho_iqz.copy()
        dphi_array = self.dphi_iqz.copy()

        # Expand on potential and density basis function
        # to get spatial detendence
        for iq in range(self.mynq):
            chi_ij = chi_qij[iq]
            Vind_qi = np.dot(chi_ij, np.array(const_per))
            rhoind_qz[iq] = np.dot(drho_array[:, iq].T, Vind_qi)
            Vind_qz[iq] = np.dot(dphi_array[:, iq].T, Vind_qi)

        rhoind_qz = self.collect_qw(rhoind_qz)
        return self.z_z, rhoind_qz, Vind_qz, self.z0_l

    @timer('Calculate plasmon eigenmodes')
    def get_plasmon_eigenmodes(self, filename=None):
        """
        Diagonalize the dieletric matrix to get the plasmon eigenresonances
        of the system.

        Returns:
            Eigenvalue array (shape Nq x nw x dim), z-grid, induced densities,
            induced potentials, energies at zero crossings.
        """

        raise NotImplementedError('Not refactored!')
        assert self.hs.qcomm.size == 1
        # eps_qwij = self.get_eps_matrix()

        Nw = len(self.omega_w)
        Nq = self.mynq
        w_w = self.omega_w
        eig = np.zeros([Nq, Nw, self.dim], dtype=complex)
        abseps = np.zeros([Nq, Nw], dtype=complex)
        # vec = np.zeros([Nq, Nw, self.dim, self.dim],
        #                dtype=complex)

        # import scipy as sp
        omega0 = [[] for i in range(Nq)]

        rho_z = [np.zeros([0, len(self.z_z)]) for i in range(Nq)]
        phi_z = [np.zeros([0, len(self.z_z)]) for i in range(Nq)]
        for iq in range(Nq):
            if (1 + iq) % (Nq // 10) == 0:
                print('{}%'.format(np.round((1 + iq) / Nq, 1) * 100))

            eps_qwij = self.get_eps_matrix(iq_q=[iq])
            if iq == 0:
                print('Calculating plasmon modes')

            eig[iq], vec_wij = np.linalg.eig(eps_qwij[0])
            abseps[iq, :] = np.linalg.det(eps_qwij[0])

            vec_dual_wij = np.linalg.inv(vec_wij)
            iwref = 0
            for iw in range(1, Nw):
                vec = vec_wij[iw]
                vec_dual = vec_dual_wij[iwref]
                overlap = np.dot(vec_dual, vec)
                index = list(np.argsort(np.abs(overlap))[:, -1])
                iwref = iw
                if len(np.unique(index)) < self.dim:  # add missing indices
                    addlist = []
                    removelist = []
                    for j in range(self.dim):
                        if index.count(j) < 1:
                            addlist.append(j)
                        if index.count(j) > 1:
                            for l in range(1, index.count(j)):
                                removelist.append(
                                    np.argwhere(np.array(index) == j)[l])
                    for j in range(len(addlist)):
                        index[removelist[j][0]] = addlist[j]

                # Sort vectors
                vec_wij[iw, :] = np.copy(vec_wij[iw][:, index])
                vec_dual_wij[iw, :] = np.copy(vec_dual_wij[iw][index, :])

                eig[iq, iw, :] = np.copy(eig[iq, iw][index])
                klist = [k for k in range(self.dim)
                         if (eig[iq, iw - 1, k] < 0 and eig[iq, iw, k] > 0)]
                for k in klist:  # Eigenvalue crossing
                    a = np.real((eig[iq, iw, k] - eig[iq, iw - 1, k]) /
                                (w_w[iw] - w_w[iw - 1]))
                    # linear interp for crossing point
                    w0 = np.real(-eig[iq, iw - 1, k]) / a + w_w[iw - 1]
                    rho = np.dot(self.drho_iqz[:, iq, :].T,
                                 vec_dual_wij[iw, k, :])
                    phi = np.dot(self.dphi_iqz[:, iq, :].T,
                                 vec_wij[iw, :, k])
                    rho_z[iq] = np.append(rho_z[iq], rho[np.newaxis, :],
                                          axis=0)
                    phi_z[iq] = np.append(phi_z[iq], phi[np.newaxis, :],
                                          axis=0)
                    omega0[iq].append(w0)

        # Make eigenfrequencies more easy to work with
        nmodes = 0
        for freqs in omega0:
            nmodes = np.max([len(freqs), nmodes])

        freqs_qm = np.zeros((Nq, nmodes), float) + np.nan

        for freqs_m, omega0_m in zip(freqs_qm, omega0):
            freqs_m[:len(omega0_m)] = np.sort(omega0_m)

        z = self.z_z
        if filename is not None:
            q_q = self.q_q
            omega_w = self.omega_w
            data = {'eig': eig, 'z': z,
                    'rho_z': rho_z, 'freqs_qm': freqs_qm,
                    'q_q': q_q, 'omega_w': omega_w,
                    'abseps': abseps}
            np.savez_compressed(filename, **data)

        return eig, z, rho_z, phi_z, freqs_qm, abseps


def plot_plasmons(hs, output,
                  plot_eigenvalues=False,
                  plot_density=False,
                  plot_potential=False,
                  save_plots=None,
                  show=True):
    raise NotImplementedError('Not refactored!')
    eig, z, rho_z, phi_z, omega0, abseps = output

    import matplotlib.pyplot as plt
    q_q = hs.q_q
    nq = len(q_q)
    omega_w = hs.frequencies

    plt.figure()
    plt.title('Plasmon modes')
    for iq in range(nq):
        freqs = np.array(omega0[iq])
        plt.plot([q_q[iq], ] * len(freqs), freqs, 'k.')
        plt.ylabel(r'$\hbar\omega$ (eV)')
        plt.xlabel(r'q (Å$^{-1}$)')
    if save_plots is not None:
        plt.savefig('plasmon_modes' + str(save_plots))

    plt.figure()
    plt.title('Loss Function')
    loss_qw = np.sum(-np.imag(eig**(-1)), axis=-1)
    plt.pcolor(q_q, omega_w, loss_qw.T, vmax=10)
    plt.ylabel(r'$\hbar\omega$ (eV)')
    plt.xlabel(r'q (Å$^{-1}$)')
    plt.colorbar()
    if save_plots is not None:
        plt.savefig('loss' + str(save_plots))

    if plot_eigenvalues:
        plt.figure()
        for iq in range(0, nq, nq // 10):
            plt.plot(omega_w, eig[iq].real)
            plt.plot(omega_w, eig[iq].imag, '--')
        if save_plots is not None:
            plt.savefig('Eigenvalues' + str(save_plots))

    if plot_potential:
        plt.figure()
        plt.title('Induced potential')
        q = nq // 5
        pots = np.array(phi_z[q]).real
        plt.plot(z, pots.T)
        plt.xlabel(r'z $(\AA)$')
        if save_plots is not None:
            plt.savefig('Potential' + str(save_plots))

    if plot_density:
        plt.figure()
        plt.title('Induced density')
        q = nq // 5
        dens = np.array(rho_z[q]).real
        plt.plot(z, dens.T)
        plt.xlabel(r'z $(\AA)$')
        if save_plots is not None:
            plt.savefig('Density' + str(save_plots))


def make_heterostructure(layers,
                         frequencies=[0.001, 5.0, 5000],
                         momenta=[0.0001, 5.0, 2000],
                         thicknesses=None,
                         substrate=None):
    """Easy function for making a heterostructure based on some layers"""
    from qeh.materialparameters import default_ehmasses, default_thicknesses
    raise NotImplementedError('Not refactored!')
    # Copy for internal handling
    layers = layers.copy()
    # layers = expand_layers(layers)

    # Remove those annoying '-chi.npz'
    for il, layer in enumerate(layers):
        if '-chi.npz' in layer:
            layers[il] = layer[:-8]

    # Parse input for layer
    originallayers = []  # Unmodified layer identifiers
    layerargs = []  # Modifiers for layers like +phonons and +doping

    for layer in layers:
        tmp = layer.split('+')
        name, layerargs = tmp[0], tmp[1:]
        originallayers.append(name)
        layerargs.append(layerargs)

    if thicknesses is None:
        thicknesses = []
        for layer in layers:
            for key in default_thicknesses:
                if '-icsd-' in key:
                    key2 = key.split('-icsd-')[0]
                else:
                    key2 = key

                if key2 in layer:
                    thicknesses.append(default_thicknesses[key])
                    break
            else:
                raise NotImplementedError
                print('define the thickness of all layers')

    q_q = np.linspace(*momenta)
    omega_w = np.linspace(*frequencies)
    # Interpolate the building blocks such that they are
    # represented on the same q and omega grid
    print('Interpolating building blocks to same grid')
    interpolate_building_blocks(BBfiles=originallayers, q_grid=q_q,
                                w_grid=omega_w)

    # The layers now have appended an '_int'
    for il, layer in enumerate(layers):
        ind = layer.find('+')
        if ind < 0:
            layers[il] = layer + '_int'
            continue

        layers[il] = layer[:ind] + '_int' + layer[ind:]

    # Parse args and modify building blocks accordingly
    from qeh.buildingblocks import (GrapheneBB,
                                    dopedsemiconductor,
                                    phonon_polarizability)
    for layer in set(layers):
        # Everything that comes after a '+' is a modifier
        tmp = layer.split('+')
        modifiers = tmp[1:]

        if not len(modifiers):
            continue

        origin = tmp[0]
        originpath = origin + '-chi.npz'

        bb = np.load(originpath)

        for im, modifier in enumerate(modifiers):
            if 'doping' in modifier:
                subargs = modifier.split(',')
                kwargs = {'doping': 0,
                          'temperature': 0,
                          'eta': 1e-4,
                          'direction': 'x'}

                # Try to find emass in default values
                for key, val in default_ehmasses.items():
                    if origin[:-4] in key:
                        kwargs['effectivemass'] = val['emass1']

                # Overwrite
                for subarg in subargs:
                    key, value = subarg.split('=')
                    if key == 'T':
                        key = 'temperature'
                    elif key == 'em':
                        key = 'effectivemass'
                    elif key == 'direction':
                        try:
                            kwargs[key] = float(value)
                        except ValueError:
                            kwargs[key] = value
                        continue
                    kwargs[key] = float(value)

                mod = ['{}={}'.format(str(key), str(val))
                       for key, val in kwargs.items()]
                modifiers[im] = ','.join(mod)

                if 'graphene' in layer:
                    # Treat graphene specially, since in this case we are using
                    # an analytical approximation of the building block
                    assert np.allclose(kwargs['temperature'], 0.0), \
                        print('Graphene currently cannot be at a finite temp.')
                    bb = GrapheneBB(bb, doping=kwargs['doping'],
                                    eta=kwargs['eta'])
                else:
                    bb = dopedsemiconductor(bb, **kwargs)

            if 'phonons' in modifier:
                phonons = Path(origin[:-4] + '-phonons.npz')
                subargs = modifier.split(',')
                overwrite_masses = {}
                eta = 0.1e-3
                # Overwrite
                for subarg in subargs:
                    if '=' in subarg:
                        key, value = subarg.split('=')
                        if key == 'eta':
                            eta = float(value)
                        else:
                            overwrite_masses[key] = float(value)

                if not phonons.exists():
                    continue
                dct = np.load(str(phonons))

                Z_avv, C_NN, masses, cell = (dct['Z_avv'],
                                             dct['C_NN'],
                                             dct['masses'],
                                             dct['cell'])

                bb = phonon_polarizability(bb, Z_avv, masses, C_NN, cell,
                                           overwrite_masses=overwrite_masses,
                                           gamma=eta)

        # Save modified building block
        newlayer = '{}+{}'.format(origin, '+'.join(modifiers))
        newlayerpath = newlayer + '-chi.npz'
        np.savez_compressed(newlayerpath, **bb)

        for il, layer2 in enumerate(layers):
            if layer2 == layer:
                layers[il] = newlayer

    thicknesses = np.array(thicknesses)

    # Calculate distance between layers
    d = (thicknesses[1:] + thicknesses[:-1]) / 2
    d0 = thicknesses[0]

    # Print summary of structure
    print('Structure:')
    if substrate is not None:
        print('    Substrate d = {} Å'.format(substrate['d'][0]))
    for thickness, layer in zip(thicknesses, layers):
        print('    {} d = {} Å'.format(layer, thickness))
    het = QEH(structure=layers, d=d,
              thicknesses=thicknesses, d0=d0,
              substrate=substrate)
    return het


def list_building_blocks(defpath):
    from qeh.materialparameters import default_ehmasses, default_thicknesses
    bbs = list(defpath.glob('*.npz'))
    columns = []
    for bb in bbs:
        row = ['', '', '', '', '']
        filename = bb.name
        if not filename.endswith('-chi.npz'):
            continue
        material = filename.split('-chi.npz')[0]
        row[0] = material

        for key in default_thicknesses:
            if 'icsd' in key:
                key2 = key.split('-icsd-')[0]
            else:
                key2 = key
            if key2 == material:
                thickness = default_thicknesses[key]
                row[1] = f'{thickness} Å'
                break

        if (defpath / Path(f'{material}-phonons.npz')).is_file():
            row[2] = 'available'

        for key, val in default_ehmasses.items():
            if key.startswith(material):
                emass = val['emass1']
                hmass = val['hmass1']
                row[3] = f'{emass} me'
                row[4] = f'{hmass} me'
        columns.append(row)

    columns.insert(0, ['Name', 'Thickness', 'phonons', 'e_mass (d)', 'h_mass'])
    widths = [0, 0, 0, 0, 0]
    for row in columns:
        for ir, elem in enumerate(row):
            widths[ir] = max([len(elem), widths[ir]])

    descriptions = []
    for row in columns:
        description = ''
        for elem, width in zip(row, widths):
            description += f'{elem:<{width}} | '
        descriptions.append(description)

    header = descriptions.pop(0)
    print(header)
    print('-' * len(header))
    descriptions.sort()

    for description in descriptions:
        print(description)


def download_bb(target):
    import tarfile
    import urllib.request
    targz = str(target) + '.tar.gz'
    url = 'https://cmr.fysik.dtu.dk/_downloads/chi-data-v2.tar.gz'
    urllib.request.urlretrieve(url, targz)
    tar = tarfile.open(targz, "r:gz")
    tar.extractall(str(target.parent))
    tar.close()


def main(args=None):
    import argparse
    from pathlib import Path
    from os.path import expanduser
    import shutil

    description = 'QEH Model command line interface'

    example_text = """examples:
    (In the following "qeh = python -m qeh". It can be nice
    to set this as an alias in your bashrc.)

    Calculate graphene plasmons with doping and plot them:
        qeh graphene+doping=0.5 --plasmons --plot

    Same but 5 layers of graphene and save to numpy npz file:
        qeh 5graphene+doping=0.5 --plasmons --plasmonfile plasmons.npz

    Graphene-Boron Nitride-Graphene heterostructure with doping and phonons:
        qeh graphene+doping=0.5 3BN+phonons graphene+doping=0.5

    Set up doped MoS2 at finite temperatures (T is in eV) with a
    custom relaxation rate:
        qeh H-MoS2+doping=0.1,T=25e-3,eta=1e-3,em=0.43

    Set custom omega and q grid:
        qeh 2graphene+doping=0.5 --q 1e-3 0.1 100 --omega 1e-3 2 1000

    """
    formatter = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(description=description,
                                     epilog=example_text,
                                     formatter_class=formatter)

    help = """
    '3H-MoS2 graphene 10H-WS2' gives 3 layers of H-MoS2,
    1 layer of graphene and 10 layers of H-WS2. Each layer can be further
    modified (e.g. adding doping and phonon contribution) by appending
    +doping=0.1 or +phonons.

    For example 3 layers of 0.1 eV doped MoS2 (with effective mass m*=0.43)
    can be modelled by '3H-MoS2+doping=0.1,em=0.43'. Phonon contributions
    are added as '3H-MoS2+phonons'. Additional arguments such as the
    temperature, effective masses, relaxation rate can also be added.
    Please see the provided examples in the bottom.
    """

    parser.add_argument('layers', nargs='*', help=help, type=str)
    help = ("For above example: '6.2 3.2 6.2' gives thicknesses of "
            "6.2 3.2 and 6.2 AA to MoS2, graphene and WS2 "
            "respectively. If not set, the QEH module will use a "
            "default set of thicknesses")
    parser.add_argument('--thicknesses', nargs='*', help=help,
                        default=None, type=float)

    defpath = Path(__file__).parent / 'chi-data'
    help = ("Path to folder containing dielectric building blocks. "
            f"Defaults to current folder, ./chi-data and {defpath} "
            "in that order")
    parser.add_argument('--buildingblockpath', nargs='*', help=help,
                        default=['.', './chi-data', defpath], type=str)

    help = ("Calculate plasmon spectrum")
    parser.add_argument('--plasmons', action='store_true', help=help)

    help = ("Calculate eigenvalues of dielectric matrix")
    parser.add_argument('--eigenvalues', action='store_true', help=help)

    help = ("Calculate induced potential for finite q")
    parser.add_argument('--potential', action='store_true', help=help)

    help = ("Calculate induced density for finite q")
    parser.add_argument('--density', action='store_true', help=help)

    help = ("Save plasmon modes to file")
    parser.add_argument('--plasmonfile', type=str, default=None, help=help)

    help = ("Calculate eels spectrum")
    parser.add_argument('--eels', type=str, default=None, help=help)

    help = ("Plot calculated quantities")
    parser.add_argument('--plot', action='store_true', help=help)

    help = ("Save plots to file")
    parser.add_argument('--save-plots', type=str, default=None, help=help)

    help = ("Add a substrate to the structure."
            "A file '-sub.npz' that contains the dielectric function of the "
            "substrate, (in x and z direction in the anisotropic case)"
            "the omega grid where the epsilon is defined, the distance"
            "of the substrate to the bottom most layer must to be given"
            "and a True or False 'isotropic' argument."
            "E. g.: --substrate SiO2")
    parser.add_argument('--substrate', type=str, default=None, help=help)

    help = ("Custom frequencies to represent quantities on (in eV). "
            "The format is: min. frequency, max. frequency, "
            "number of frequencies. E. g.: 0.001 1.0 500")
    parser.add_argument('--omega', default=[0.001, 1.0, 500],
                        nargs=3,
                        help=help, type=float)

    help = ("Custom momentas to respresent quantities on (in AA^-1). "
            "The format is: min. q, max. q, number of q's. "
            "E. g.: 0.0001 0.15 100")
    parser.add_argument('--q', default=[0.0001, 0.15, 200],
                        nargs=3,
                        help=help, type=float)

    help = 'List the available data and building blocks'
    parser.add_argument('--list', help=help, action='store_true')

    args = parser.parse_args(args)

    layers = args.layers
    paths = args.buildingblockpath
    if args.save_plots is None:
        save_plots = ''
    else:
        save_plots = args.save_plots

    if not defpath.is_dir():
        print('First run of QEH package, downloading dielectric bb\'s - '
              'this could take a couple of minutes.')
        download_bb(defpath)
        print('Done.')

    if args.list:
        return list_building_blocks(defpath)

    # layers = expand_layers(layers)
    # Locate files for layers
    print('Looking for building blocks')
    layer_paths = []
    for il, layer in enumerate(layers):
        # Parse layer and its arguments
        tmp = layer.split('+')
        layer = tmp[0]

        layerpath = layer + '-chi.npz'
        p = Path(layerpath)
        for path in paths:
            bb = Path(expanduser(path)) / p
            if bb.is_file():
                break
        else:
            msg = 'Building block files ({bb}) cannot be found!'
            raise FileNotFoundError(msg)

        layer_paths.append(str(bb))

    # Copy files to current directory
    for layerpath in set(layer_paths):
        p = Path(layerpath)
        layer = str(p.name).split('-chi')[0]

        src = str(p)
        dest = str(p.name)

        if src != dest:
            print(('Copying building block '
                   'to current folder from {}').format(str(p)))
            shutil.copy(src, dest)

        # Also copy phonons if present
        phonons = (Path(layerpath).parent /
                   Path('{}-phonons.npz'.format(layer)))
        if not phonons.exists():
            continue
        src = str(phonons)
        dest = str(phonons.name)
        if src != dest:
            shutil.copy(src, dest)

    if args.substrate:
        substrate = np.load('{}-sub.npz'.format(str(args.substrate)))
    else:
        substrate = None

    # Make QEH calculation
    print('Initializing heterostructure')
    hs = make_heterostructure(layers,
                              thicknesses=args.thicknesses,
                              momenta=args.q,
                              frequencies=args.omega,
                              substrate=substrate)

    import matplotlib.pyplot as plt

    if args.plasmons:
        print('Calculate plasmon spectrum')
        tmp = hs.get_plasmon_eigenmodes(filename=args.plasmonfile)
        plot_plasmons(hs, tmp, plot_eigenvalues=args.eigenvalues,
                      plot_potential=args.potential,
                      plot_density=args.density,
                      save_plots=save_plots,
                      show=False)
    if args.eels:
        q_q, frequencies, eels_qw = hs.get_eels(dipole_contribution=True)

    hs.timer.write()

    if args.plot:
        plt.show()
    else:
        plt.close('all')


if __name__ == '__main__':
    main()
