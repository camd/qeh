import numpy as np
from typing import Union
from functools import cached_property
from pathlib import Path
import warnings
from ase.utils.timing import timer, Timer

from qeh.buildingblock import BuildingBlock, Layer
from qeh.substrate import Substrate
from qeh.qwcomm import QComm, QCommMPI
from qeh.tools.io import read


class Heterostructure:
    def __init__(self,
                 BBfiles: list[str],
                 layerwidth_n: Union[list[float], np.ndarray, None] = None,
                 layerwidth_l: Union[list[float], np.ndarray, None] = None,
                 qmax: Union[float, None] = None,
                 wmax: Union[float, None] = None,
                 amax: Union[int, None] = None,
                 dz: Union[float, None] = None,
                 substrate: Union[Substrate, None] = None,
                 qcomm: Union[QComm, None] = None,
                 include_overlap=True):
        '''
        Initialize the heterostructure from a list of building block
        files. The building blocks should be on the same grid. The
        layers are defined by the thicknesses in layerwidth_n.

        Parameters
        ----------
        BBfiles: list of str
            list of names of the building block files
        layerwidth_n: list of float | None
            list of the thicknesses of the building blocks (unit: Bohr)
        layerwidth_l: list of float | None
            list of the thicknesses of the layers (unit: Bohr)
        qmax: float | None
            maximum q value of the heterostructure (unit: 1/Bohr)
        wmax: float | None
            maximum frequency value of the heterostructure (unit: Hartree)
        amax: int | None
            maximum number of basis functions in the building blocks
        dz: float | None
            distance between neighoring points in the heterostructure z-grid
            unit(Bohr)
        substrate: Substrate | None
            substrate descriptor object
        qcomm: QComm | None
            q-communicator. If None, then it defaults to
            distributing q-points over all ranks.

        Class responsibilities
        ----------------------
        - Calculations in terms of the z_z grid.
        - Looping over layers.
        '''
        self.timer = Timer()
        self.substrate = substrate
        self.include_overlap = include_overlap
        BBfiles = expand_layers(BBfiles)

        if qcomm is None:
            bb0 = BuildingBlock.from_file(BBfiles[0], qmax=qmax)
            q_Q = bb0.q_q
            QN = len(q_Q)
            qcomm = QCommMPI(QN)
        self.qcomm = qcomm

        BB_n, BBindex_l, self.qcomm = \
            self.bbs_from_files(BBfiles,
                                qcomm=qcomm,
                                qmax=qmax,
                                wmax=wmax,
                                amax=amax,
                                timer=self.timer)

        if layerwidth_n is None and layerwidth_l is None:
            self.layerwidth_l = get_default_thicknesses(BBfiles)
        elif layerwidth_l is None:
            layerwidth_n = [layerwidth for layerwidth in layerwidth_n]
            self.layerwidth_l = np.array([layerwidth_n[n] for n in BBindex_l])
        elif layerwidth_n is None:
            self.layerwidth_l = np.array(layerwidth_l)
        else:
            raise AttributeError('Do not specify both' +
                                 ' layerwidth_n and layerwidth_l')

        if substrate is not None:
            assert substrate.dist > self.layerwidth_l[0] / 2, \
                'Distance to substrate must be at least' \
                'half the width of the first layer'
        self.z_z, self.dz, z0_l = self.get_z_grid(BB_n, dz)
        self.z0_l = z0_l
        self.layers_l = [Layer(z0, d, self.z_z, BB_n[n]) for z0, n, d
                         in zip(z0_l, BBindex_l, self.layerwidth_l)]

        self.nlayers = len(self.layers_l)
        self.ndim = sum([l.bb.aN for l in self.layers_l])

        self.q_q = self.layers_l[0].bb.q_q
        self.qN = len(self.q_q)
        self.omega_w = self.layers_l[0].bb.omega_w
        self.wN = len(self.omega_w)

    def get_z_grid(self, BB_n, dz=None):
        """
        Construct z-grid for entire heterostructure.
        dz is the distance between points in the grid. If not specified, we
        choose dz to make the grid twice as dense as the densest z-grid used
        in any of the building blocks.
        The grid is centered such that edge of the first layer starts at z=0.

        """
        width_hs = np.sum(self.layerwidth_l)
        # extra space below and above heterostructure
        edgesize_below = BB_n[0].Z_Z.max() - BB_n[0].Z_Z.min()
        edgesize_above = BB_n[-1].Z_Z.max() - BB_n[-1].Z_Z.min()
        edgesize_total = edgesize_below + edgesize_above
        system_size = width_hs + edgesize_total
        width_min = np.min(self.layerwidth_l)
        if dz is None:  # determine dz from building block grids
            dz = BB_n[0].dZ / 2
            for BB in BB_n:
                dz1 = BB.dZ / 2
                dz = min(dz, dz1)
        if width_min / dz < 10:
            warnings.warn('Warning: z-grid may be too coarse. At least one '
                          'building block is covered by less than 10 grid '
                          'points!')
        if width_min / dz > 1000:
            warnings.warn(f'Warning: z-grid (dz = {dz}) is very dense. You may'
                          ' be able to save time and memory by specifying dz')

        z_z = np.arange(0, system_size, dz) - edgesize_below
        z0_l = np.cumsum(self.layerwidth_l) - self.layerwidth_l / 2

        return z_z, dz, z0_l

    @timer('BB setup')
    def bbs_from_files(self,
                       BBfiles: list,
                       qcomm: QComm,
                       qmax: Union[float, None] = None,
                       wmax: Union[float, None] = None,
                       amax: Union[int, None] = None,
                       timer: Union[Timer, None] = None):
        ''' Read building blocks from files

        Parameters
        ----------
        BBfiles: list of str or Path
            list of names of the building block files
        qmax: float | None
            maximum q value of the heterostructure
        wmax: float | None
            maximum frequency value of the heterostructure
        amax: int | None
            maximum number of basis functions in the building blocks
        timer: Timer | None
            timer

        Returns
        -------
        BB_n: list of BuildingBlock
            list of the building blocks
        BBindex_l: list of int
            list of the building blocks
        qcomm: QComm
            q communicator
        '''

        # We use dict.fromkeys instead of set, to preserve order
        namelist_n = list(dict.fromkeys(BBfiles).keys())  # unique bb names.

        all_bb_grids_are_identical = check_building_blocks(namelist_n)
        if not all_bb_grids_are_identical:
            raise ValueError('Building Blocks not on the same grid')

        # construct name -> layer index mapping
        BBtypes_l = []
        for l, name in enumerate(BBfiles):
            BBtypes_l.append(namelist_n.index(name))
        BBtypes_l = np.array(BBtypes_l)

        BB_n = []
        for filename in namelist_n:
            bb = BuildingBlock.from_file(filename,
                                         qcomm=qcomm,
                                         qmax=qmax,
                                         wmax=wmax,
                                         amax=amax,
                                         timer=timer)
            BB_n.append(bb)

        return BB_n, BBtypes_l, qcomm

    @cached_property
    def drho_qzi(self):
        return self.get_interpolated_densities()

    @cached_property
    def phi_qiz(self):
        return self.get_basis_functions()

    @cached_property
    def dphi_qzi(self):
        return self.get_induced_potentials()

    @cached_property
    def grho_qij(self):
        return self.get_grho_qij(include_overlap=self.include_overlap)

    @cached_property
    def gphi_qij(self):
        return self.get_gphi_qij()

    @cached_property
    def chi_intra_qwij(self):
        return self.get_chi_intra()

    @cached_property
    def kernel_qwij(self) -> np.ndarray:
        return self.get_Coulomb_kernel()

    @timer('Interpolate densities')
    def get_interpolated_densities(self):
        """
        Returns the density basis functions for the entire heterostructure
        on a z-grid.
        """
        drho_qzi = np.zeros((self.qN, len(self.z_z), self.ndim), dtype=complex)
        index = 0  # index in the layer + basis dimension
        for layer in self.layers_l:
            drho_qzi[:, :, index:index + layer.bb.aN] = layer.drho_qza
            index += layer.bb.aN  # add the bb number of basis functions

        return drho_qzi

    @timer('Setup basis functions')
    def get_basis_functions(self):
        """
        Returns potential basis functions for the entire heterostructure
        on a z-grid.
        """
        phi_qiz = np.zeros((self.qN, self.ndim, len(self.z_z)), dtype=complex)
        index = 0  # index in the layer + basis dimension
        for layer in self.layers_l:
            phi_qiz[:, index:index + layer.bb.aN] = layer.phi_qaz
            index += layer.bb.aN  # add the bb number of basis functions

        return phi_qiz

    @timer('Get induced potentials')
    def get_induced_potentials(self):
        """
        Calculates the potentials induced by the density basis functions
        for each layer by solving the 1D Poisson equation.

        The Poisson equation is solved once for every type of building block
        in the heterostructure. It is first solved on the building-block z-grid
        and then interpolated to the common heterostructure z-grid.
        """
        zN = len(self.z_z)
        dphi_qzi = np.zeros([self.qN, zN, self.ndim], dtype=complex)
        index = 0  # index in the layer + basis dimension
        for l, layer in enumerate(self.layers_l):
            dphi_qzi[:, :, index:index + layer.bb.aN] = layer.dphi_qza
            index += layer.bb.aN  # add the bb number of basis functions

        return dphi_qzi

    @timer('Get chi_intra')
    def get_chi_intra(self):
        """
        Returns the chi_intra matrix for the entire heterostructure
        in the basis <rho | chi_intra | phi>
        chi_intra is is constructed as a block-diagonal matrix with one
        block for each layer. Each block has shape aN x aN, where aN is the
        number of basis functions in the layer.

        Since density basis functions of different layers may overlap,
        chi_intra may not actually be block-diagonal. To correct this, we
        replace the block-diagonal rho metric by the full rho metric.
        """
        chi_intra_qwij = np.zeros((self.qN, self.wN, self.ndim, self.ndim),
                                  dtype=complex)
        index = 0  # index in the layer + basis dimension
        for l, layer in enumerate(self.layers_l):
            chi_intra_qwij[:, :, index:index + layer.bb.aN,
                           index:index + layer.bb.aN] = layer.bb.chi_qwab
            index += layer.bb.aN  # add the bb number of basis functions
        # replace block-diagonal metric by full metric
        overlap_qij = np.linalg.inv(self.grho_qij)
        g_intra_qij = self.get_grho_qij(include_overlap=False)
        chi_intra_qwij = overlap_qij[:, None] @ g_intra_qij[:, None]\
            @ chi_intra_qwij
        return chi_intra_qwij

    @timer('Get grho_qij')
    def get_grho_qij(self, include_overlap=True):
        r"""
        Calculates the (inverse) overlap matrix of the density basis functions.
        (grho^{-1})_{qij} = <rho_i(q) | rho_j(q)>.
        If the keyword include_overlap is False, the overlap between
        basis functions of neighboring layers will be neglected. In this case,
        the overlap matrix is block-diagonal, with one a_l x a_l block for each
        layer
        """
        self.timer.start('Matrix multiplication')
        grho_inv_qij = self.drho_qzi.transpose((0, 2, 1)).conj() @ \
            self.drho_qzi * self.dz
        self.timer.stop('Matrix multiplication')
        self.timer.start('Matrix inversion')
        if include_overlap:
            grho_qij = np.linalg.inv(grho_inv_qij)
        else:
            grho_qij = np.zeros_like(grho_inv_qij)
            index = 0  # index in the layer + basis dimension
            for l, layer in enumerate(self.layers_l):
                grho_qij[:, index:index + layer.bb.aN,
                         index:index + layer.bb.aN] = \
                    np.linalg.inv(grho_inv_qij[:, index:index + layer.bb.aN,
                                               index:index + layer.bb.aN])
                index += layer.bb.aN  # add the bb number of basis functions

        self.timer.stop('Matrix inversion')
        return grho_qij

    @timer('Get gphi_qij')
    def get_gphi_qij(self):
        r"""
        Calculates the (inverse) overlap matrix of the potential basis
        functions. (gphi^{-1})_{ij} = <phi_i | phi_j>.
        """
        gphi_inv_qij = self.phi_qiz @ self.phi_qiz.transpose(0, 2, 1) * self.dz
        gphi_qij = np.linalg.inv(gphi_inv_qij)
        return gphi_qij

    @timer('Project on rho basis')
    def project_on_rho(self, f_z, iq=0, return_coefficients=False):
        r"""
        Project function f_z on the induced densities at the q'th q-point.
        """
        assert len(f_z) == len(self.z_z)
        dz = self.dz
        drho_iz = self.drho_qzi[iq].T
        grho_ij = self.grho_qij[iq]
        coefs_i = grho_ij @ drho_iz.conj() @ f_z * dz
        if return_coefficients:
            return coefs_i
        else:
            return coefs_i @ drho_iz

    @timer('Project on phi basis')
    def project_on_phi(self, f_z, iq=0, return_coefficients=False):
        r"""
        Project function f_z on the potential basis functions
        at the q'th q-point
        """
        assert len(f_z) == len(self.z_z)
        dz = self.dz
        phi_iz = self.phi_qiz[iq]
        gphi_ij = self.gphi_qij[iq]
        coefs_i = gphi_ij @ phi_iz.conj() @ f_z * dz
        if return_coefficients:
            return coefs_i
        else:
            return coefs_i @ phi_iz

    @timer('Get monopole potential')
    def get_monopole_potential(self, iq=0, normalize=False):
        """
        Find the expansion coefficients of the monopole potential V(z) = 1
        expressed in terms of the potential basis functions
        """
        V_z = np.ones(self.z_z.shape)
        V_i = self.project_on_phi(V_z, iq=iq, return_coefficients=True)
        if normalize:
            norm = np.sqrt(V_i @ np.linalg.solve(self.gphi_qij[iq], V_i))
            V_i = V_i / norm
        return V_i

    @timer('Get dipole potential')
    def get_dipole_potential(self, iq=0, normalize=False):
        """
        Find the expansion coefficients of the dipole potential V(z) = (z-zM)
        expressed in terms of the potential basis functions.
        Here zM is defined as the "center of mass" of the heterostructure.
        """
        z0_l = np.array([layer.z0 for layer in self.layers_l])
        zM = z0_l.mean()
        V_z = self.z_z - zM
        V_i = self.project_on_phi(V_z, iq=iq, return_coefficients=True)
        if normalize:
            norm = np.sqrt(V_i @ np.linalg.solve(self.gphi_qij[iq], V_i))
            V_i = V_i / norm
        return V_i

    def multiply_chi_intra(self, A_qwij, B_qwij):
        '''
        Efficiently calculates the matrix C_qwij = A_qwij @ chi_qwij @ B_qwij
        by summing over all layers. Not sure if useful though.
        '''
        C_qwij = np.zeros((self.qN, self.wN, self.ndim, self.ndim),
                          dtype=complex)
        index = 0  # index in the layer + basis dimension
        for l, layer in enumerate(self.layers_l):
            C_qwij += A_qwij[:, :, :, index:index + layer.bb.aN] @ \
                layer.bb.chi_qwab @ \
                B_qwij[:, :, index:index + layer.bb.aN, :]
            index += layer.bb.aN  # add the bb number of basis functions

        return C_qwij

    @timer('Get coulomb kernel')
    def get_Coulomb_kernel(self):
        kernel_qij = self.phi_qiz @ self.dphi_qzi * self.dz
        kernel_qwij = kernel_qij[:, None]
        if self.substrate is not None:
            kernelsub_qwij = self.substrate.get_Coulomb_kernel(self)
            kernel_qwij = kernel_qwij + kernelsub_qwij
        return kernel_qwij

    @timer('Get interlayer Coulomb kernel')
    def get_interlayer_Coulomb_kernel(self):
        kernel_qij = self.phi_qiz @ self.dphi_qzi * self.dz
        i = 0
        for l, layer in enumerate(self.layers_l):
            aN = layer.bb.aN  # number of basis functions in layer
            # remove intralayer interaction
            kernel_qij[:, i:i + aN, i:i + aN] = 0
            i += aN

        kernel_qwij = kernel_qij[:, None]
        if self.substrate is not None:
            kernelsub_qwij = self.substrate.get_Coulomb_kernel(self)
            kernel_qwij = kernel_qwij + kernelsub_qwij

        return kernel_qwij


def get_default_thicknesses(BBfiles):
    from qeh.materialparameters import default_thicknesses
    layerwidth_l = []
    for bbpath in BBfiles:
        # remove file extension and file location from name
        bb = Path(bbpath).stem
        if bb[-3:] == '-bb':
            bb = bb[:-3]
        elif bb[-4:] == '-chi':
            bb = bb[:-4]
        for key in default_thicknesses:
            flag = True
            if bb in key:
                layerwidth_l.append(default_thicknesses[key])
                flag = False
                break
        if flag:
            raise KeyError(f'No default thickness for {bb}')
    return np.array(layerwidth_l)


def expand_layers(structure):
    newlist = []
    for name in structure:
        if isinstance(name, str):
            num = ''
            while name[0].isdigit():
                num += name[0]
                name = name[1:]
            try:
                num = int(num)
            except ValueError:
                num = 1
            for n in range(num):
                newlist.append(name)
        else:
            newlist.append(name)
    return newlist


def check_building_blocks(BBfiles=None):
    """ Check that building blocks are on same frequency-
    and q- grid.

    BBfiles: list of str
        list of names of BB files
    """
    name = BBfiles[0]
    data = read(name)
    try:
        q = data['q_abs'].copy()
        w = data['omega_w'].copy()
    except KeyError:
        try:
            q = data['q_q'].copy()
            w = data['omega_w'].copy()
        except KeyError as err:
            raise KeyError(f'Unknown file format: {name}') from err

    for name in BBfiles[1:]:
        data = read(name)
        try:
            q2 = data['q_abs'].copy()
            w2 = data['omega_w'].copy()
        except KeyError:
            try:
                q2 = data['q_q'].copy()
                w2 = data['omega_w'].copy()
            except KeyError as err:
                raise KeyError(f'Unknown file format: {name}') from err
        if not ((q2 == q).all and
                (w2 == w).all):
            return False
    return True
